/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

/**
 *
 * @author Jacob
 */
public class InfluenceTemplate {
    String name;
    String effect;
    Float effectAmount;
    Float effectRadius;
    Float effectLife;
    
    public InfluenceTemplate(String name, String effect, Float effectAmount, Float effectRadius, Float effectLife) {
        this.name = name;
        this.effect = effect;
        this.effectAmount = effectAmount; 
        this.effectRadius = effectRadius;
        this.effectLife = effectLife;
    }
    
}
