package mygame;

import com.jme3.asset.AssetManager;
import com.jme3.audio.AudioNode;
import com.jme3.bullet.control.BetterCharacterControl;
import com.jme3.collision.CollisionResults;
import com.jme3.export.JmeExporter;
import com.jme3.export.JmeImporter;
import com.jme3.export.Savable;
import com.jme3.input.InputManager;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.AnalogListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.controls.MouseAxisTrigger;
import com.jme3.input.controls.MouseButtonTrigger;
import com.jme3.math.Plane;
import com.jme3.math.Ray;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;
import com.jme3.scene.control.Control;
import com.jme3.terrain.geomipmap.TerrainQuad;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class PlayerControl extends AbstractControl implements Savable, Cloneable {

    private String mouseOrGamepadOrKeyboard = "mouse";

    public boolean left = false, right = false, up = false, down = false, isRunning = true, cycleInfluenceItems = false, useCurrentSpawnItem = false, cycleSpawnItems = false, jump = false, crouch = false, targetleft = false, targetright = false, targetup = false, targetdown = false;
    public Float analogLeftRight = 0f, analogUpDown = 0f, analogTargetLeftRight = 0f, analogTargetUpDown = 0f;

    private Float speed = 20f;
    private Float targetSpeed = 30f;

    private Float targetMovementMultiplier = 10f;
    private Float currentSpawnItemCooldown = 0f;
    //private Float cycleSpawnItemCooldownLimit = 5f;
    private Integer currentInfluenceItemIndex = 0;
    private Integer currentSpawnItemIndex = 0;
    private Vector3f walkDirection = new Vector3f(0, 0, 0);
    private Vector3f targetOffset = new Vector3f(0, 0, 0);
    Vector3f mouseLocation = new Vector3f(0, 0, 0);
    Vector3f targetBefore = new Vector3f(0, 0, 0);

    InfluenceItemManager influenceItemManager;
    InputManager inputManager;
    InGameObjectTemplateManager inGameObjectTemplateManager;
    AssetManager assetManager;

    Spatial target;
    UserSettings userSettings;
    Integer joystickDevice;
    Camera cam;
    TerrainQuad terrain;
    boolean isMomentaryZoom = false;
    ArrayList<SpawningItemInventory> spawningItems;
    ArrayList<InfluenceItemInventory> influenceItems; /* Influence items I have */

    ArrayList<String> possibleInfluenceItems; /* Influenceitems the current spawn item can spawn */

    Node soundEmitter;

    Boolean inCar = false;

    public PlayerControl(InfluenceItemManager influenceItemManager, InputManager inputManager, Spatial target, UserSettings userSettings, Integer joystickDevice, Camera cam, TerrainQuad terrain, ArrayList<SpawningItemInventory> spawningItems, ArrayList<InfluenceItemInventory> influenceItems, InGameObjectTemplateManager inGameObjectTemplateManager, AssetManager assetManager, Node soundEmitter) {
        this.influenceItemManager = influenceItemManager;
        this.inputManager = inputManager;
        this.inGameObjectTemplateManager = inGameObjectTemplateManager;
        this.assetManager = assetManager;

        this.target = target;
        this.userSettings = userSettings;
        this.joystickDevice = joystickDevice;
        this.cam = cam;
        this.terrain = terrain;
        this.spawningItems = spawningItems;
        this.influenceItems = influenceItems;
        this.soundEmitter = soundEmitter;

        this.initKeys();

        this.RefreshPossibleInfluenceItems();
        this.NextInfluenceItem();

    }

    /**
     * Custom Keybinding: Map named actions to inputs.
     */
    private void initKeys() {
        // You can map one or several inputs to one named actions
        inputManager.addMapping("Left", new KeyTrigger(userSettings.getInteger("keyboardMapLeft")));
        inputManager.addMapping("Right", new KeyTrigger(userSettings.getInteger("keyboardMapRight")));
        inputManager.addMapping("Up", new KeyTrigger(userSettings.getInteger("keyboardMapUp")));
        inputManager.addMapping("Down", new KeyTrigger(userSettings.getInteger("keyboardMapDown")));

        inputManager.addMapping("TargetLeft", new KeyTrigger(userSettings.getInteger("keyboardMapTargetLeft")));
        inputManager.addMapping("TargetRight", new KeyTrigger(userSettings.getInteger("keyboardMapTargetRight")));
        inputManager.addMapping("TargetUp", new KeyTrigger(userSettings.getInteger("keyboardMapTargetUp")));
        inputManager.addMapping("TargetDown", new KeyTrigger(userSettings.getInteger("keyboardMapTargetDown")));

        inputManager.addMapping("Jump", new KeyTrigger(userSettings.getInteger("keyboardMapJump")));
        inputManager.addMapping("CycleInfluenceItems", new KeyTrigger(userSettings.getInteger("keyboardMapCycleInfluenceItems")));
        inputManager.addMapping("UseCurrentSpawnItem", new KeyTrigger(userSettings.getInteger("keyboardMapUseCurrentSpawnItem")));
        inputManager.addMapping("CycleSpawnItems", new KeyTrigger(userSettings.getInteger("keyboardMapCycleSpawnItems")));

        inputManager.addMapping("MouseMoveXPos", new MouseAxisTrigger(com.jme3.input.MouseInput.AXIS_X, false));
        inputManager.addMapping("MouseMoveXNeg", new MouseAxisTrigger(com.jme3.input.MouseInput.AXIS_X, true));
        inputManager.addMapping("MouseMoveYPos", new MouseAxisTrigger(com.jme3.input.MouseInput.AXIS_Y, false));
        inputManager.addMapping("MouseMoveYNeg", new MouseAxisTrigger(com.jme3.input.MouseInput.AXIS_Y, true));

        inputManager.addMapping("UseCurrentSpawnItem", new MouseButtonTrigger(userSettings.getInteger("mouseMapUseCurrentSpawnItem")));
        inputManager.addMapping("CycleInfluenceItems", new MouseButtonTrigger(userSettings.getInteger("mouseMapCycleInfluenceItems")));

        inputManager.addListener(actionListener, "Left");
        inputManager.addListener(actionListener, "Right");
        inputManager.addListener(actionListener, "Up");
        inputManager.addListener(actionListener, "Down");
        inputManager.addListener(actionListener, "TargetLeft");
        inputManager.addListener(actionListener, "TargetRight");
        inputManager.addListener(actionListener, "TargetUp");
        inputManager.addListener(actionListener, "TargetDown");
        inputManager.addListener(actionListener, "Jump");

        inputManager.addListener(actionListener, "UseCurrentSpawnItem");
        inputManager.addListener(actionListener, "CycleInfluenceItems");
        inputManager.addListener(actionListener, "CycleSpawnItems");

        inputManager.addListener(analogMouseListener, "MouseMoveXPos");
        inputManager.addListener(analogMouseListener, "MouseMoveXNeg");
        inputManager.addListener(analogMouseListener, "MouseMoveYPos");
        inputManager.addListener(analogMouseListener, "MouseMoveYNeg");

    }

    /**
     * This method is called when the control is added to the spatial, and when
     * the control is removed from the spatial (setting a null value). It can be
     * used for both initialization and cleanup.
     *
     * @param spatial
     */
    @Override
    public void setSpatial(Spatial spatial) {
        super.setSpatial(spatial);
        /* Example:
         if (spatial != null){
         // initialize
         }else{
         // cleanup
         }
         */
    }

    /**
     * Implement your spatial's behaviour here. From here you can modify the
     * scene graph and the spatial (transform them, get and set userdata, etc).
     * This loop controls the spatial while the Control is enabled.
     *
     * @param tpf
     */
    @Override
    protected void controlUpdate(float tpf) {
        //Vector3f location = spatial.getControl(BetterCharacterControl.class).getPhysicsSpace().
        //System.out.println("target location " + target.getLocalTranslation());
        /* Lets deal with the player input */
        walkDirection.x = 0;
        walkDirection.z = 0;

        Float localLeftRight = 0f;
        Float localUpDown = 0f;
        Vector3f analogTarget = new Vector3f(analogTargetLeftRight, 0f, analogTargetUpDown).normalize();

        if (left) {
            localLeftRight = 1f;
        } else if (right) {
            localLeftRight = -1f;
        } else {
            localLeftRight = analogLeftRight;
        }
        if (up) {
            localUpDown = 1f;
        } else if (down) {
            localUpDown = -1f;
        } else {
            localUpDown = analogUpDown;
        }

        if (targetleft) {
            setTargetOffsetX(targetOffset.x + (targetSpeed * tpf), "keypressed left");
        } else if (targetright) {
            setTargetOffsetX(targetOffset.x - (targetSpeed * tpf), "keypressed right");
        } else {
            if (mouseOrGamepadOrKeyboard.equals("gamepad")) {
                if (isMomentaryZoom) {
                    setTargetOffsetX(targetOffset.x + (analogTargetLeftRight * (targetSpeed * tpf)), "Analog moved leftright while zoomed");
                } else {
                    setTargetOffsetX(targetOffset.x = analogTarget.x * 20, "Analog moved leftright while not zoomed");
                }
            }
        }
        if (targetup) {
            setTargetOffsetZ(targetOffset.z + (targetSpeed * tpf), "keypressed up");
        } else if (targetdown) {
            setTargetOffsetZ(targetOffset.z - (targetSpeed * tpf), "keypressed down");
        } else {
            if (mouseOrGamepadOrKeyboard.equals("gamepad")) {
                if (isMomentaryZoom) {
                    setTargetOffsetZ(targetOffset.z + (analogTargetUpDown * (targetSpeed * tpf)), "Analog moved updown while zoomed");
                } else {
                    setTargetOffsetZ(analogTarget.z * 20, "Analog moved updown while not zoomed");
                }
            }
        }

        if (useCurrentSpawnItem) {
            /* the cooldown doesn't work, so, one at a time */
            useCurrentSpawnItem = false;

            //System.out.println("############ useCurrentSpawnItem currentSpawnItemCooldown " + currentSpawnItemCooldown + " spawningItems.get(currentSpawnItemIndex).getCooldown() " + spawningItems.get(currentSpawnItemIndex).getCooldown() + " tpf " + tpf);
            if (currentSpawnItemCooldown == 0) {
                useSpawnItem(currentSpawnItemIndex);
            } else if (currentSpawnItemCooldown > spawningItems.get(currentSpawnItemIndex).getCooldown()) {
                //System.out.println("currentSpawnItemCooldown |" + currentSpawnItemCooldown + "| reached limit | " + spawningItems.get(currentSpawnItemIndex).getCooldown() + "|");
                currentSpawnItemCooldown = 0f;
            } else {
                //System.out.println("currentSpawnItemCooldown in effect |" + currentSpawnItemCooldown + "| reached limit | " + spawningItems.get(currentSpawnItemIndex).getCooldown() + "|");
                currentSpawnItemCooldown += tpf;
            }

        }
        if (cycleSpawnItems) {
            cycleSpawnItems = false;
//            System.out.println("");
//            System.out.println("===================================================================");
//            System.out.println("--  cycleSpawnItems  --");
            NextSpawnItem();
        }
        if (cycleInfluenceItems) {
            cycleInfluenceItems = false;
//            System.out.println("");
//            System.out.println("===================================================================");
//            System.out.println("--  cycleInfluenceItems  --");
            NextInfluenceItem();
        }
        if (jump) {

        }
        if (crouch) {

        }

        if (analogLeftRight != 0.0 || analogUpDown != 0) {
            //System.out.println("analogLeftRight |" + analogLeftRight + "| analogUpDown |" + analogUpDown + "|");
        }

        walkDirection.x = localLeftRight * speed;
        walkDirection.z = localUpDown * speed;

        /* Lets move the target */
        /* Lets set the locations etc.... of the player and target */
        //target.setLocalTranslation(target.getLocalTranslation().x + (analogLeftRight * targetMovementMultiplier),target.getLocalTranslation().y,target.getLocalTranslation().z + (analogUpDown * targetMovementMultiplier));
        //target.setLocalTranslation(mouseLocation);
        /* get the target offset from the player, move the player, then set the target offest */
        //Vector3f targetOffset = spatial.getLocalTranslation().subtract(target.getLocalTranslation());
        spatial.getControl(BetterCharacterControl.class).setWalkDirection(walkDirection);

        Float targetheight = findTargetHeightOnTerrain(spatial.getLocalTranslation().add(targetOffset));
        setTargetOffsetY(targetheight - spatial.getLocalTranslation().y, "Setting target height of terrain");
        //targetOffset.y = targetheight - spatial.getLocalTranslation().y;

        target.setLocalTranslation(targetOffset);

        /* if its currently intersecting, don't move it any more */
        //Vector3f targetLocation = target.getLocalTranslation();
        //target.setLocalTranslation(targetOffset);
        /*
         private static final int LEFT_PLANE = 0;
         private static final int RIGHT_PLANE = 1;
         private static final int BOTTOM_PLANE = 2;
         private static final int TOP_PLANE = 3; 
         */
        Plane leftplane = cam.getWorldPlane(0);
        Plane rightplane = cam.getWorldPlane(1);
        Plane bottomplane = cam.getWorldPlane(2);
        Plane topplane = cam.getWorldPlane(3);

        if (Plane.Side.valueOf("Negative") == leftplane.whichSide(target.getWorldTranslation())) {
            //System.out.println("We've left the left side of the screen");
            setTargetOffsetX(targetBefore.getX(), "Left left of screen");
        } else if (Plane.Side.valueOf("Negative") == rightplane.whichSide(target.getWorldTranslation())) {
            //System.out.println("We've left the right side of the screen");
            setTargetOffsetX(targetBefore.getX(), "Left right of screen");
        } else {
            targetBefore.setX(targetOffset.getX());
        }

        if (Plane.Side.valueOf("Negative") == topplane.whichSide(target.getWorldTranslation())) {
            //System.out.println("We've left the top side of the screen");
            setTargetOffsetZ(targetBefore.getZ(), "Left top of screen");
        } else if (Plane.Side.valueOf("Negative") == bottomplane.whichSide(target.getWorldTranslation())) {
            //System.out.println("We've left the bottom side of the screen");
            setTargetOffsetZ(targetBefore.getZ(), "Left bottom of screen");
        } else {
            targetBefore.setZ(targetOffset.getZ());
        }

//          
//        if(target.getLastFrustumIntersection().equals(Camera.FrustumIntersect.Intersects.Inside)){
//            System.out.println("target is Inside FrustrumIntersect, setting targetBefore to " + targetOffset);
//            targetBefore.setX(targetOffset.getX());
//            targetBefore.setY(targetOffset.getY());
//            targetBefore.setZ(targetOffset.getZ());
//        } else {
//            System.out.println("target is Outside FrustrumIntersect, leaving targetBefore alone " + targetBefore);
//            
//            targetOffset.setX(targetBefore.getX());
//            targetOffset.setY(targetBefore.getY());
//            targetOffset.setZ(targetBefore.getZ());
//            target.setLocalTranslation(targetBefore);
//        }
//        
//        } else {
//            System.out.println("target is not Inside FrustrumIntersect setting targetOffset to " + targetBefore);
//            targetOffset = targetBefore;
//            target.setLocalTranslation(targetOffset);
//            
//            System.out.println("targetBefore " + targetBefore);
//        }
        //System.out.println("targetOffset " + targetOffset);
        /* now that we have the correct x and z, we need to work out the correct y - where the target should sit on the ground */
        //spatial.getControl(BetterCharacterControl.class).setViewDirection(target.getLocalTranslation());
    }

    @Override
    public Control cloneForSpatial(Spatial spatial) {
        final PlayerControl control = new PlayerControl(influenceItemManager, inputManager, target, userSettings, joystickDevice, cam, terrain, spawningItems, influenceItems, inGameObjectTemplateManager, assetManager, soundEmitter);
        /* Optional: use setters to copy userdata into the cloned control */
        // control.setIndex(i); // example
        control.setSpatial(spatial);
        return control;
    }

    @Override
    protected void controlRender(RenderManager rm, ViewPort vp) {
        /* Optional: rendering manipulation (for advanced users) */
    }

    @Override
    public void read(JmeImporter im) throws IOException {
        super.read(im);
        // im.getCapsule(this).read(...);
    }

    @Override
    public void write(JmeExporter ex) throws IOException {
        super.write(ex);
        // ex.getCapsule(this).write(...);
    }

    private Float findTargetHeightOnTerrain(Vector3f targetpos) {
        /* default result to 0,0,0 just in case it doesn't collide with anything */
        Vector3f result = new Vector3f(0, 0, 0);
        CollisionResults results = new CollisionResults();

        // Aim the ray down, if there are no collisions then we aim the ray up and try again        
        Ray rayDown = new Ray(targetpos, new Vector3f(0, 1, 0));
        Ray rayUp = new Ray(targetpos, new Vector3f(0, -1, 0));

        // Collect intersections between ray and all nodes in results list.
        terrain.collideWith(rayDown, results);

        if (results.size() > 0) {
            result = results.getClosestCollision().getContactPoint();
        } else {
            terrain.collideWith(rayUp, results);
            if (results.size() > 0) {
                result = results.getClosestCollision().getContactPoint();
            }
        }

        return result.y;
    }

    public void setTargetOffset(Vector3f offset, String name) {
        setTargetOffsetX(offset.getX(), name);
        setTargetOffsetY(offset.getY(), name);
        setTargetOffsetZ(offset.getZ(), name);
    }

    public void setTargetOffsetX(Float offset, String name) {
//        if (offset == 0f) {
//            System.out.println(name + " setting X to " + offset);
//        }
        targetOffset.setX(offset);
    }

    public void setTargetOffsetY(Float offset, String name) {
//        if (offset == 0f) {
//            System.out.println(name + " setting Y to " + offset);
//        }
        targetOffset.setY(offset);
    }

    public void setTargetOffsetZ(Float offset, String name) {
//        if (offset == 0f) {
//            System.out.println(name + " setting Z to " + offset);
//        }
        targetOffset.setZ(offset);
    }

    public String getMouseOrGamepadOrKeyboard() {
        return mouseOrGamepadOrKeyboard;
    }

    public void setMouseOrGamepadOrKeyboard(String mouseOrGamepadOrKeyboard) {
        this.mouseOrGamepadOrKeyboard = mouseOrGamepadOrKeyboard;
    }

    public SpawningItemInventory getSpawnItem() {
        return spawningItems.get(currentSpawnItemIndex);
    }

    public InfluenceItemInventory getInfluenceItem() {
        return influenceItems.get(currentInfluenceItemIndex);
    }

    private void NextSpawnItem() {
        //System.out.println("NextSpawnItem currentSpawnItemIndex|" + currentSpawnItemIndex + "| spawningItems.size() |" + spawningItems.size() + "|");
        currentSpawnItemIndex++;

        if (currentSpawnItemIndex >= spawningItems.size()) {
            currentSpawnItemIndex = 0;
        }

        //System.out.println("######## NextSpawnItem is " + spawningItems.get(currentSpawnItemIndex).getName());
        RefreshPossibleInfluenceItems();
        NextInfluenceItem();
    }

    /* according to the curentSpawnItem, find next appropriette influenceItem to spawn */
    private void NextInfluenceItem() {
        Boolean found = false;
        currentInfluenceItemIndex++;

//        for (int j = 0; j < influenceItems.size(); j++) {
//            System.out.println(" j " + j + " influenceitem " + influenceItems.get(j).getName() + " x " + influenceItems.get(j).getAmount());
//        }
//        System.out.println("");
//        Iterator<String> possibleInfluenceItemsIteratordebug = possibleInfluenceItems.iterator();
//
//        while (possibleInfluenceItemsIteratordebug.hasNext()) {
//            String possibleInfluenceItem = possibleInfluenceItemsIteratordebug.next();
//
//            System.out.println("possibleinfluenceitem " + possibleInfluenceItem);
//        }
        /* go through our influence items, looking for the first one that is in possibleInfluenceItems */
        for (int i = currentInfluenceItemIndex, j = 0; j < influenceItems.size() && !found; i++, j++) {

            if (i >= influenceItems.size()) {
                //System.out.println("  (resetting i)" + i);
                i = 0;
            }

            //System.out.println("  i " + i + " j " + j + " influenceitem " + influenceItems.get(i).getName());
            Iterator<String> possibleInfluenceItemsIterator = possibleInfluenceItems.iterator();

            while (possibleInfluenceItemsIterator.hasNext() && !found) {
                String possibleInfluenceItem = possibleInfluenceItemsIterator.next();

                //System.out.println("    Is our current influenceitem " + influenceItems.get(i).getName() + " equal to this? " + possibleInfluenceItem);
                if (influenceItems.get(i).getName().equals(possibleInfluenceItem)) {
                    //System.out.println("     Found item " + possibleInfluenceItem + " at index " + i);
                    found = true;
                    currentInfluenceItemIndex = i;
                }
            }
        }

        //System.out.println("#### Found next influence item for spawing item " + spawningItems.get(currentSpawnItemIndex).getName() + " is " + influenceItems.get(currentInfluenceItemIndex).getName() + " and we have " + influenceItems.get(currentInfluenceItemIndex).getAmount() + " of them");
    }

    /* according to the curentSpawnItem, find first appropriette influenceItem to spawn */
    private void RefreshPossibleInfluenceItems() {
        //System.out.println("Finding possibleInfluenceItems for " + spawningItems.get(currentSpawnItemIndex).getName() + " at index " + currentSpawnItemIndex);
        possibleInfluenceItems = inGameObjectTemplateManager.getInfluenceItems(spawningItems.get(currentSpawnItemIndex).getName());
        currentInfluenceItemIndex = -1;
    }

    private void useSpawnItem(Integer spawnItemIndex) {

        if (influenceItems.get(currentInfluenceItemIndex).getAmount() == 0) {
            //System.out.println("  Cannot use " + spawningItems.get(spawnItemIndex).getName() + ", out of ammo");

            AudioNode spawnSound = new AudioNode(assetManager, spawningItems.get(spawnItemIndex).getEmptySound(), false);
            /* TODO: setPositional true, but, need mono audio */
            spawnSound.setPositional(false);
            spawnSound.setLooping(false);
            spawnSound.setVolume(2);
            soundEmitter.attachChild(spawnSound);
            spawnSound.playInstance();
            soundEmitter.detachChild(spawnSound);
        } else {
            //System.out.println("  Using " + spawningItems.get(spawnItemIndex).getName() + " with this sound " + spawningItems.get(spawnItemIndex).getSound());
            /* infinite use items have an amount of -1, so lets not bother decrementing the amount */
            if(!(influenceItems.get(currentInfluenceItemIndex).getAmount() < 0)){
                influenceItems.get(currentInfluenceItemIndex).setAmount(influenceItems.get(currentInfluenceItemIndex).getAmount() - 1);
            }
            
            AudioNode spawnSound = new AudioNode(assetManager, spawningItems.get(spawnItemIndex).getSound(), false);
            /* TODO: setPositional true, but, need mono audio */
            spawnSound.setPositional(false);
            spawnSound.setLooping(false);
            spawnSound.setVolume(2);
            soundEmitter.attachChild(spawnSound);
            spawnSound.playInstance();
            soundEmitter.detachChild(spawnSound);

            //System.out.println("  Spawning an influenceitem called " + influenceItems.get(currentInfluenceItemIndex).getName());
            influenceItemManager.spawn((Node) spatial, target.getLocalTranslation().normalize(), influenceItems.get(currentInfluenceItemIndex));
        }
    }

    public Boolean getInCar() {
        return inCar;
    }

    public void setInCar(Boolean inCar) {
        this.inCar = inCar;
    }

    private ActionListener actionListener = new ActionListener() {
        public void onAction(String name, boolean keyPressed, float tpf) {
            //S1ystem.out.println("Key pressed onAction name |" + name + "| keyPressed | " + keyPressed + "|");
            if (!getInCar()) {
                if (name.equals("Left")) {
                    left = keyPressed;
                } else if (name.equals("Right")) {
                    right = keyPressed;
                } else if (name.equals("Up")) {
                    up = keyPressed;
                } else if (name.equals("Down")) {
                    down = keyPressed;
                } else if (name.equals("TargetLeft")) {
                    targetleft = keyPressed;
                    setMouseOrGamepadOrKeyboard("keyboard");
                } else if (name.equals("TargetRight")) {
                    targetright = keyPressed;
                    setMouseOrGamepadOrKeyboard("keyboard");
                } else if (name.equals("TargetUp")) {
                    targetup = keyPressed;
                    setMouseOrGamepadOrKeyboard("keyboard");
                } else if (name.equals("TargetDown")) {
                    targetdown = keyPressed;
                    setMouseOrGamepadOrKeyboard("keyboard");
                } else if (name.equals("Jump")) {
                    jump = keyPressed;
                } else if (name.equals("UseCurrentSpawnItem")) {
                    useCurrentSpawnItem = keyPressed;
                } else if (name.equals("CycleInfluenceItems")) {
                    cycleInfluenceItems = keyPressed;
                } else if (name.equals("CycleSpawnItems")) {
                    cycleSpawnItems = keyPressed;
                }
            }
        }
    };

    private AnalogListener analogMouseListener = new AnalogListener() {
        public void onAnalog(String name, float value, float tpf) {
            setMouseOrGamepadOrKeyboard("mouse");
            /* two unsatisfactory ways of mousing */
            if (false) {
                /*first way*/
                Float distancex = 0.0f;
                Float distancez = 0.0f;

                if (name.equals("MouseMoveXPos")) {
                    distancex = -(value * tpf * 100000);
                } else if (name.equals("MouseMoveXNeg")) {
                    distancex = (value * tpf * 100000);
                } else if (name.equals("MouseMoveYPos")) {
                    distancez = (value * tpf * 100000);
                } else if (name.equals("MouseMoveYNeg")) {
                    distancez = -(value * tpf * 100000);
                }

                setTargetOffsetX(targetOffset.getX() + distancex, "Mousemoved old way");
                setTargetOffsetZ(targetOffset.getZ() + distancez, "Mousemoved old way");

                //targetOffset.x = targetOffset.x + distancex;
                //targetOffset.z = targetOffset.z + distancez;
            } else {
                /*second way*/
                //System.out.println("onAnalog|" + targetOffset + "|name |" + name + "| value |" + new BigDecimal(value).toPlainString() + "| tpf |" + tpf);            /* mouse moved, lets work out where its pointed */

                CollisionResults results = new CollisionResults();
                Vector3f mouseOffset;
                // Convert screen click to 3d position
                Vector2f click2d = inputManager.getCursorPosition();
                Vector3f click3d = cam.getWorldCoordinates(new Vector2f(click2d.x, click2d.y), 0f).clone();
                Vector3f dir = cam.getWorldCoordinates(new Vector2f(click2d.x, click2d.y), 1f).subtractLocal(click3d).normalizeLocal();

                // Aim the ray from the clicked spot forwards.
                Ray ray = new Ray(click3d, dir);

                // Collect intersections between ray and all nodes in `results list.
                terrain.collideWith(ray, results);

                if (results.size() > 0) {
                    mouseLocation = results.getClosestCollision().getContactPoint();

                    setTargetOffset(mouseLocation.subtract(spatial.getWorldTranslation()), "Mousemoved new way");
                }
            }
        }
    };

}
