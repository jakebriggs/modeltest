/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.asset.AssetManager;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.control.BetterCharacterControl;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import java.util.UUID;

/**
 *
 * @author Jacob
 */
public class StaticModel {
    Node staticModelNode;

    String type;
    StaticModelManager staticModelManager;
    AssetManager assetManager;
    Node nodeToAttachTo;
    Vector3f startlocation;
    InGameObjectTemplateManager inGameObjectTemplateManager;
    UUID id;
    BulletAppState bulletAppState;

    public StaticModel(StaticModelManager staticModelManager, BulletAppState bulletAppState,AssetManager assetManager, Node nodeToAttachTo, Vector3f startlocation, String type, InGameObjectTemplateManager inGameObjectTemplateManager, UUID id) {

        this.type = type;
        this.staticModelManager = staticModelManager;
        this.assetManager = assetManager;
        this.nodeToAttachTo = nodeToAttachTo;
        this.startlocation = startlocation;
        this.inGameObjectTemplateManager = inGameObjectTemplateManager;
        this.bulletAppState = bulletAppState;
        this.id = id;
        BetterCharacterControl betterCharacterControl;
        String model;

        if (type == null) {
            type = "generic";
        }

        
        if (type.equals("tree")) {
            model = "Models/Tree/Tree.mesh.xml";
            betterCharacterControl = new BetterCharacterControl(1f, 10f, 800);
        } else if (type.equals("boat")) {
            model = "Models/Boat/boat.mesh.xml";
            
            betterCharacterControl = new BetterCharacterControl(8f, 0.5f, 800);
        } else {
            model = "Models/" + type + "/" + type + ".mesh.xml";
            betterCharacterControl = new BetterCharacterControl(10f, 10f, 800);
        }

        Spatial spatial = assetManager.loadModel(model);
        spatial.scale(4f);
        spatial.setName(type + " spatial ");
        Node spatialNode = (Node) spatial;
        spatialNode.setName(type + "spatialNode " + UUID.randomUUID().toString());

        staticModelNode = new Node(id.toString());
        staticModelNode.attachChild(spatialNode);

        staticModelNode.move(0, 0, 0);

        staticModelNode.addControl(betterCharacterControl);

        betterCharacterControl.setJumpForce(new Vector3f(0, 0f, 0));
        betterCharacterControl.setGravity(new Vector3f(0, 10000f, 0));
        betterCharacterControl.setApplyPhysicsLocal(true);
        betterCharacterControl.warp(startlocation); // warp character into landscape at particular location

        bulletAppState.getPhysicsSpace().add(betterCharacterControl);
        bulletAppState.getPhysicsSpace().addAll(staticModelNode);

        nodeToAttachTo.attachChild(staticModelNode);
        staticModelNode.setLocalTranslation(startlocation.x,-10,startlocation.z);

    }
}
