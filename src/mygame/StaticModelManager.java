/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.asset.AssetManager;
import com.jme3.bullet.BulletAppState;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.system.AppSettings;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;
import java.util.UUID;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;

/**
 *
 * @author Jacob
 */
public class StaticModelManager {

    private Node staticModelNode;
    private BulletAppState bulletAppState;
    private AssetManager assetManager;
    // private InfluenceItemManager influenceItemManager;
    private InGameObjectTemplateManager inGameObjectTemplateManager;
    private AppSettings staticmodelsettings;
    private String settingsfile = "InitialGameState/StaticModels.xml";

    private ArrayList<StaticModel> staticModels = new ArrayList<StaticModel>();

    public StaticModelManager(Node staticModelNode, BulletAppState bulletAppState, AssetManager assetManager, InGameObjectTemplateManager inGameObjectTemplateManager) {
        this.staticModelNode = staticModelNode;
        this.assetManager = assetManager;
        this.inGameObjectTemplateManager = inGameObjectTemplateManager;
        this.bulletAppState = bulletAppState;

    }

    public void load() {
        Document staticModelFile = (Document) assetManager.loadAsset(settingsfile);
        processStaticModels(staticModelFile.getDocumentElement());
        //randomStaticModels(100);
        
        System.out.println("StaticModelManager LOADED");
    }

    public void think() {

    }

    public void spawn(Vector3f location, String faction, String type) {
        staticModels.add(new StaticModel(this, bulletAppState, assetManager, staticModelNode, location, type, inGameObjectTemplateManager, UUID.randomUUID()));
    }

    /* recurse throught he DOM */
    public void processStaticModels(org.w3c.dom.Node node) {
        // do something with the current node instead of System.out
        System.out.println(node.getNodeName());

        if (node.getNodeName() == "static-model") {
            NamedNodeMap attributes = node.getAttributes();

            Float x = Float.parseFloat(attributes.getNamedItem("x").getNodeValue());
            Float y = Float.parseFloat(attributes.getNamedItem("y").getNodeValue());
            Float z = Float.parseFloat(attributes.getNamedItem("z").getNodeValue());
            String type = attributes.getNamedItem("type").getNodeValue();

            staticModels.add(new StaticModel(this, bulletAppState, assetManager, staticModelNode, new Vector3f(x, y, z), type, inGameObjectTemplateManager, UUID.randomUUID()));
        }

        NodeList nodeList = node.getChildNodes();
        for (int i = 0; i < nodeList.getLength(); i++) {
            org.w3c.dom.Node currentNode = nodeList.item(i);
            if (currentNode.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
                //calls this method for all the children which is Element
                processStaticModels(currentNode);
            }
        }
    }

    public Character findStaticModel(UUID id) {
        Iterator it = staticModels.iterator();

        while (it.hasNext()) {
            NPCCharacter next = (NPCCharacter) it.next();

            if (next.getId().equals(id)) {
                return next;
            }
        }

        return null;
    }

    public void manyStaticObjectsInfluenced(HashSet<UUID> ids, InfluenceInventory influence) {
        Iterator it = ids.iterator();

        while (it.hasNext()) {
            staticObjectInfluenced((UUID) it.next(), influence);
        }
    }

    public void staticObjectInfluenced(UUID id, InfluenceInventory influence) {
        /* very lazy way to find influenced character */
        System.out.println("staticObjectInfluenced " + id);

        findStaticModel(id).causeInfluence(influence);
//        Iterator it = staticModels.iterator();
//
//        while (it.hasNext()) {
//            NPCCharacter next = (NPCCharacter) it.next();
//
//            if (next.getId().equals(id)) {
//                next.causeInfluence(influence);
//            }
//        }
    }

    public void randomStaticModels(Integer number) {
        // do something with the current node instead of System.out
        Random rand = new Random();

        for (int i = 0; i <= number; i++) {

            Float x = (rand.nextFloat() * 1000) - 500;
            Float y = 10f;
            Float z = (rand.nextFloat() * 1000) - 500;
            String type = "tree";

            staticModels.add(new StaticModel(this, bulletAppState, assetManager, staticModelNode, new Vector3f(x, y, z), type, inGameObjectTemplateManager, UUID.randomUUID()));
        }
    }

}
