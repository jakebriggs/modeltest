/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.asset.AssetManager;
import com.jme3.bullet.BulletAppState;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import java.util.UUID;

/**
 *
 * @author Jacob
 */
public class NPCCharacter extends Character {

    String faction;
    String type;

    public NPCCharacter(CharacterManager characterManager, BulletAppState bulletAppState, AssetManager assetManager, InfluenceItemManager influenceItemManager, Node nodeToAttachTo, Vector3f startlocation, String faction, String type, InGameObjectTemplateManager inGameObjectTemplateManager, UUID id) {
        super(characterManager, bulletAppState, assetManager, influenceItemManager, nodeToAttachTo, startlocation, type, inGameObjectTemplateManager, id);

        this.faction = faction;
        this.type = type;

        NPCCharacterControl think = new NPCCharacterControl(characterManager, id);
        characterNode.addControl(think);

        //influenceItems.add(new InfluenceItemInventory("grenade",10));

    }
}
