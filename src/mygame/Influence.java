/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.asset.AssetManager;
import com.jme3.bounding.BoundingSphere;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.control.BetterCharacterControl;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.shape.Line;
import com.jme3.scene.shape.Sphere;

/**
 *
 * @author Jacob
 */
public class Influence {

    Node influenceNode;
    public boolean timeup = false;
    private Float time = 0f;
    private Float timeLimit = 0.5f;
    private CharacterManager characterManager;
    private Vector3f direction;

    public Influence(BulletAppState bulletAppState, AssetManager assetManager, InGameObjectTemplateManager inGameObjectTemplateManager, Node nodeToAttachTo, Node nodeToInfluence, Vector3f location, Vector3f direction, String type, CharacterManager characterManager) {
        //System.out.println("direction " + direction);
        this.characterManager = characterManager;
        this.direction = direction;
        Geometry anInfluenceArea;
        InfluenceInventory influence = inGameObjectTemplateManager.getInfluence(type);
        /* use type to get effectradius and name and effect and other stuff */

        //System.out.println("I influence " + nodeToInfluence.getName() + " I am an influence called " + influence.getName() + " I do " + influence.getEffectAmount() + " " + influence.getEffect() + "  for " + influence.getEffectLife() + " seconds, with a range of " + influence.getEffectRange() + " using a " + influence.getMethod());
        if (influence.getMethod().equals("sphere")) {
            System.out.println("Making a sphere");
            anInfluenceArea = new Geometry(influence.getName(), new Sphere(100, 100, influence.getEffectRange()));
        } else if (influence.getMethod().equals("ray")) {
            //System.out.println("Making a ray - before mod " + this.direction);
            //anInfluenceArea = new Geometry(influence.getName(), new Line(location, direction.normalize().mult(influence.getEffectRange())) );

            this.direction.setX(this.direction.getX() * influence.getEffectRange());
            this.direction.setZ(this.direction.getZ() * influence.getEffectRange());
            this.direction.setY(this.direction.getY() + 1);

            anInfluenceArea = new Geometry(influence.getName(), new Line(new Vector3f(0, +1, 0), direction));

           // anInfluenceArea = new Geometry(influence.getName(), new Line(new Vector3f(0, -3, 0), direction.mult(influence.getEffectRange())));
            //anInfluenceArea = new Geometry(influence.getName(), new Line(new Vector3f(0, -3, 0), new Vector3f(10, -3, 10)));
        } else {
            System.out.println("Making a default sphere");
            anInfluenceArea = new Geometry(influence.getName(), new Sphere(100, 100, influence.getEffectRange()));
        }

        Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        mat.setColor("Color", ColorRGBA.Red);
        anInfluenceArea.setMaterial(mat);

        influenceNode = new Node(influence.getName());
        influenceNode.attachChild(anInfluenceArea);

        nodeToAttachTo.attachChild(influenceNode);
        influenceNode.setLocalTranslation(location);

        InfluenceControl control = new InfluenceControl(bulletAppState, nodeToInfluence, influenceNode.getWorldTranslation(), influence, characterManager, direction);
        influenceNode.addControl(control);
    }

    public Vector3f getLocalTranslation() {
        return influenceNode.getLocalTranslation();
    }

    public void setWalkDirection(Vector3f direction) {
        influenceNode.getControl(BetterCharacterControl.class).setWalkDirection(direction);
    }
}
