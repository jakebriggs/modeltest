package mygame;

import com.jme3.bounding.BoundingBox;
import com.jme3.bounding.BoundingSphere;
import com.jme3.bullet.BulletAppState;
import com.jme3.collision.CollisionResult;
import com.jme3.collision.CollisionResults;
import com.jme3.export.JmeExporter;
import com.jme3.export.JmeImporter;
import com.jme3.export.Savable;
import com.jme3.math.Ray;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;
import com.jme3.scene.control.Control;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.UUID;

public class InfluenceControl extends AbstractControl implements Savable, Cloneable {

    private Float updateNum = 0f;
    public Boolean influenceSpent = false;
    private BulletAppState bulletAppState;
    private float speed = 1100f;
    private Node nodeToInfluence;
    private Vector3f position;
    private Vector3f direction;
    InfluenceInventory influence;
    CharacterManager characterManager;

    public InfluenceControl(BulletAppState bulletAppState, Node nodeToInfluence, Vector3f position, InfluenceInventory influence, CharacterManager characterManager, Vector3f direction) {
        this.bulletAppState = bulletAppState;
        this.nodeToInfluence = nodeToInfluence;
        this.influence = influence;
        this.position = position;
        this.characterManager = characterManager;
        this.direction = direction;


        /* sphere to begin with, but, influence.getMethod() will give us sphere, ray or arc which we should use */
    }

    /**
     * This method is called when the control is added to the spatial, and when
     * the control is removed from the spatial (setting a null value). It can be
     * used for both initialization and cleanup.
     */
    @Override
    public void setSpatial(Spatial spatial) {
        super.setSpatial(spatial);
        /* Example:
         if (spatial != null){
         // initialize
         }else{
         // cleanup
         }
         */
    }

    /**
     * Implement your spatial's behaviour here. From here you can modify the
     * scene graph and the spatial (transform them, get and set userdata, etc).
     * This loop controls the spatial while the Control is enabled.
     *
     * @param tpf
     */
    @Override
    protected void controlUpdate(float tpf) {
        if (!influenceSpent) {
            CollisionResults results = new CollisionResults();
            HashSet<UUID> ids = new HashSet();

            /* set influences time per frame so we can do damage per second rather than damage per frame */
            influence.setTpf(tpf);

            if (influence.getMethod().equals("sphere")) {
                BoundingSphere bs = new BoundingSphere(influence.getEffectRange(), position);
                nodeToInfluence.collideWith(bs, results);

                if (results.size() > 0) {
                    System.out.println("Influenced this many " + results.size());

                    Iterator it = results.iterator();

                    while (it.hasNext()) {
                        CollisionResult next = (CollisionResult) it.next();

                        ids.add(UUID.fromString(next.getGeometry().getParent().getParent().getName()));
                    }
                }
            } else if (influence.getMethod().equals("ray")) {
                Ray ray = new Ray(position, direction);
                
                //Ray ray = new Ray(new Vector3f(0,1,0),new Vector3f(50,1,50));
                
                nodeToInfluence.collideWith(ray, results);

                CollisionResult closest = results.getClosestCollision();

                if (closest != null) {
                    //System.out.println("HIT From |" + position + "| collided at |" + results.getClosestCollision() + "| ");

                    ids.add(UUID.fromString(results.getClosestCollision().getGeometry().getParent().getParent().getName()));
                } else {
                    //System.out.println("MISSED From |" + position + "| direction |" + direction + "|");
                }
            }
            characterManager.manyCharactersInfluenced(ids, influence);
        }
        
        updateNum += tpf;
       
        if (updateNum > influence.getEffectLife()) {
            //System.out.println("Influence spent updateNum " + updateNum + " influence.getEffectLife() " + influence.getEffectLife());
            influenceSpent = true;

            bulletAppState.getPhysicsSpace().removeAll(spatial);
            spatial.removeFromParent();
        }

 
    }

    @Override
    public Control cloneForSpatial(Spatial spatial) {
        final InfluenceControl control = new InfluenceControl(bulletAppState, nodeToInfluence, position, influence, characterManager, direction);
        /* Optional: use setters to copy userdata into the cloned control */
        // control.setIndex(i); // example
        System.out.println("spatial is " + spatial);
        control.setSpatial(spatial);
        return control;
    }

    @Override
    protected void controlRender(RenderManager rm, ViewPort vp) {
        /* Optional: rendering manipulation (for advanced users) */
    }

    @Override
    public void read(JmeImporter im) throws IOException {
        super.read(im);
        // im.getCapsule(this).read(...);
    }

    @Override
    public void write(JmeExporter ex) throws IOException {
        super.write(ex);
        // ex.getCapsule(this).write(...);
    }

}
