/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

/**
 *
 * @author Jacob
 */
public class SpawningItemInventory {

    String name;
    String sound;
    String emptySound;
    String model;
    Float cooldown;

    public SpawningItemInventory() {

    }

    public SpawningItemInventory(String name, String sound, String emptySound, String model, Float cooldown) {
        //System.out.println("New SpawningItemInventory name |" + name + "| sound |" + sound + "| model |" + model + "| cooldown | " + cooldown + "|");

        this.name = name;
        this.sound = sound;
        this.emptySound = emptySound;
        this.model = model;
        this.cooldown = cooldown;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSound() {
        return sound;
    }

    public void setSound(String sound) {
        this.sound = sound;
    }

    public String getEmptySound() {
        return emptySound;
    }

    public void setEmptySound(String emptySound) {
        this.emptySound = emptySound;
    }
    
    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Float getCooldown() {
        return cooldown;
    }

    public void setCooldown(Float cooldown) {
        this.cooldown = cooldown;
    }
}
