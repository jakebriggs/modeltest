/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.asset.AssetManager;
import com.jme3.audio.AudioNode;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.control.BetterCharacterControl;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.cinematic.MotionPath;
import com.jme3.cinematic.MotionPathListener;
import com.jme3.cinematic.events.MotionEvent;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.shape.Sphere;

/**
 *
 * @author Jacob
 */
public class InfluenceItem {

    Node influenceItemNode;
    InfluenceManager influenceManager;
    public boolean timeup = false;
    private Float time = 0f;
    private Float timeLimit = 3f;
    private Float radius = 0.125f;
    private Vector3f direction;
    MotionPath path;
    MotionEvent motionControl;

    public InfluenceItem(BulletAppState bulletAppState, AssetManager assetManager, InfluenceManager influenceManager, Node nodeToAttachTo, Vector3f direction, InfluenceItemInventory influenceItemInventory) {
        Geometry anInfluenceItem;
        Vector3f velocity;
        this.direction = direction;
        this.influenceManager = influenceManager;

        //System.out.println("I am a " + influenceItemInventory.getName() + " who " + influenceItemInventory.getInfluence() + " via " + influenceItemInventory.getMechanism() + " and I last " + influenceItemInventory.getTimeActive() + " and I use this model " + influenceItemInventory.getModel() + " and there are " + influenceItemInventory.getAmount() + " left");
        if (influenceItemInventory.getModel().equals("sphere")) {
            anInfluenceItem = new Geometry(influenceItemInventory.getName(), new Sphere(100, 100, radius));
        } else {
            anInfluenceItem = new Geometry(influenceItemInventory.getName(), new Sphere(100, 100, radius));
        }
        Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        mat.setColor("Color", ColorRGBA.Red);
        anInfluenceItem.setMaterial(mat);

        influenceItemNode = new Node(influenceItemInventory.getName());
        influenceItemNode.attachChild(anInfluenceItem);

        /* which direction should we start our journey? */
        Vector3f normalizedir = direction.normalize();

        if (influenceItemInventory.getPath().equals("arc") && false) {

            path = new MotionPath();
            path.addWayPoint(new Vector3f(0, 1, 0));
            
            path.addWayPoint(new Vector3f(3, 1, 3));
            path.addWayPoint(new Vector3f(10, 1, 10));
            path.addWayPoint(new Vector3f(20, 1, 20));
            path.addWayPoint(new Vector3f(0, 1, 40));
            path.addWayPoint(new Vector3f(-20, 1, 20));
            path.addWayPoint(new Vector3f(-10, 1, 10));
            
            path.enableDebugShape(assetManager, nodeToAttachTo);

            path.addListener(new MotionPathListener() {

                public void onWayPointReach(MotionEvent control, int wayPointIndex) {
                    if (path.getNbWayPoints() == wayPointIndex + 1) {
                        System.out.println(control.getSpatial().getName() + "Finished!!! ");
                    } else {
                        System.out.println(control.getSpatial().getName() + " Reached way point " + wayPointIndex);
                    }
                }
            });

            motionControl = new MotionEvent(influenceItemNode, path);
            motionControl.setDirectionType(MotionEvent.Direction.PathAndRotation);
            motionControl.setRotation(new Quaternion().fromAngleNormalAxis(-FastMath.HALF_PI, Vector3f.UNIT_Y));
            motionControl.setInitialDuration(10f);
            motionControl.setSpeed(2f);
            motionControl.setEnabled(true);
            motionControl.play();
            System.out.println("playing!");
        } else {
            RigidBodyControl influenceItemPhysics = new RigidBodyControl(1f);

            influenceItemNode.addControl(influenceItemPhysics);

            bulletAppState.getPhysicsSpace().add(influenceItemPhysics);
            bulletAppState.getPhysicsSpace().addAll(influenceItemNode);

            nodeToAttachTo.attachChild(influenceItemNode);
            influenceItemNode.setLocalTranslation(nodeToAttachTo.getLocalTranslation());

            if (influenceItemInventory.getMechanism().equals("propelled")) {
                velocity = direction.mult(25f);
                velocity.y = 2;
            } else if (influenceItemInventory.getMechanism().equals("placed")) {
                velocity = direction.mult(0f);
            } else if (influenceItemInventory.getMechanism().equals("held")) {
                velocity = direction.mult(0f);
            } else {
                velocity = direction.mult(0f);
            }

        //System.out.println("direction " + direction + " velocity " + velocity);
        /* we want to try to spawn the influenceItem just outside the character I suppose */
            /* also we don't want to aim at the ground so lets add 1 to the direction since its firing from 1 high */
            //direction.addLocal(0, 1, 0);
            influenceItemPhysics.setLinearVelocity(velocity);
            influenceItemPhysics.setPhysicsLocation(nodeToAttachTo.getLocalTranslation().add(normalizedir));
        }

        InfluenceItemControl control = new InfluenceItemControl(bulletAppState, influenceManager, influenceItemInventory, normalizedir);
        influenceItemNode.addControl(control);

    }

    public Vector3f getLocalTranslation() {
        return influenceItemNode.getLocalTranslation();
    }

    public void setWalkDirection(Vector3f direction) {
        influenceItemNode.getControl(BetterCharacterControl.class).setWalkDirection(direction);
    }

    public Vector3f getDirection() {
        return direction;
    }

    public void setDirection(Vector3f direction) {
        this.direction = direction;
    }

}
