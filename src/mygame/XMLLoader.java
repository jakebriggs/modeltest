/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.asset.AssetInfo;
import com.jme3.asset.AssetLoader;
import java.io.InputStream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;

/**
 *
 * @author Jacob
 */
public class XMLLoader implements AssetLoader {

    public Document load(AssetInfo assetInfo) {
        Document doc = createDocFromStream(assetInfo.openStream());
        if (doc != null) {
        // Process document here
            // doc.getElementsByTagName("...")
        }

        // Return an instance of your result class X here
        return doc;
    }

    public static Document createDocFromStream(InputStream inputStream) {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(inputStream);
            return doc;
        } catch (Exception ex) {
            return null;
        }
    }
}
