package mygame;

import com.jme3.input.RawInputListener;
import com.jme3.input.event.JoyAxisEvent;
import com.jme3.input.event.JoyButtonEvent;
import com.jme3.input.event.KeyInputEvent;
import com.jme3.input.event.MouseButtonEvent;
import com.jme3.input.event.MouseMotionEvent;
import com.jme3.input.event.TouchEvent;
import com.jme3.system.AppSettings;

/**
 * Easier to watch for all button and axis events with a raw input listener.
 */
public class JoystickEventListener implements RawInputListener {

    private PlayerCharacter playerCharacter;
    private Integer joystickDevice;
    private AppSettings settings;

    public JoystickEventListener(PlayerCharacter playerCharacter, Integer joystickDevice, AppSettings settings) {
        this.playerCharacter = playerCharacter;
        this.joystickDevice = joystickDevice;
        this.settings = settings;
    }

    public void onJoyAxisEvent(JoyAxisEvent evt) {
        // if this event is from the joystick we care about
        if (evt.getAxis().getJoystick().getJoyId() == joystickDevice) {
            //System.out.println("Joystick deadzone " + evt.getAxis().getLogicalId() + " axis " + evt.getAxis().getName() + " value " + evt.getValue());

            Integer axisId = evt.getAxis().getAxisId();
            // value is inverted to what I want....
            Float value = -evt.getValue();

            //pretend dead zone - do this better! An Axis can have a deadzone I think.
            //if we are in the deadzone, don't move
            if (value < 0.15f && value > -0.15) {
                value = 0f;
                //System.out.println("Joystick resetting " + value + " axis " + evt.getAxis().getName() + " id " + evt.getAxis().getAxisId());
            } else {
                //System.out.println("Joystick value " + value + " axis " + evt.getAxis().getName() + " id " + evt.getAxis().getAxisId());
            }
            
            playerCharacter.setMouseOrGamepadOrKeyboard("gamepad");

            if (axisId == settings.getInteger("joystickMapLeftRight")) {
                playerCharacter.setLeftRight(value);
            } else if (axisId == settings.getInteger("joystickMapUpDown")) {
                playerCharacter.setUpDown(value);
            } else if (axisId == settings.getInteger("joystickMapTargetLeftRight")) {
                playerCharacter.setTargetLeftRight(value);
            } else if (axisId == settings.getInteger("joystickMapTargetUpDown")) {
                playerCharacter.setTargetUpDown(value);
            }

        }

    }

    public void onJoyButtonEvent(JoyButtonEvent evt) {
        //System.out.println("Joystick " + evt.getButton().getJoystick() + " button " + evt.getButton() + " value " + evt.isPressed());
        Integer butind = evt.getButtonIndex();
        Boolean isPressed = evt.isPressed();

        if (butind == settings.getInteger("joyMapUseCurrentSpawnItem")) {
            playerCharacter.setUseCurrentSpawnItem(isPressed);
        } else if (butind == settings.getInteger("joyMapCycleInfluenceItems")) {
            playerCharacter.setCycleInfluenceItems(isPressed);
        } else if (butind == settings.getInteger("joyMapJump")) {
            playerCharacter.setJump(isPressed);
        } else if (butind == settings.getInteger("joyMapCycleSpawnItems")) {
            playerCharacter.setCycleSpawnItems(isPressed);
        } else if (butind == settings.getInteger("joyMapCouch")) {
            playerCharacter.setCrouch(isPressed);
        }
    }

    public void beginInput() {
    }

    public void endInput() {
    }

    public void onMouseMotionEvent(MouseMotionEvent evt) {
    }

    public void onMouseButtonEvent(MouseButtonEvent evt) {
    }

    public void onKeyEvent(KeyInputEvent evt) {
    }

    public void onTouchEvent(TouchEvent evt) {
    }
}
