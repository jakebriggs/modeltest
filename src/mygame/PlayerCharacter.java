/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.asset.AssetManager;
import com.jme3.bounding.BoundingBox;
import com.jme3.bullet.BulletAppState;
import com.jme3.input.InputManager;
import com.jme3.material.Material;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Box;
import com.jme3.terrain.geomipmap.TerrainQuad;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author Jacob
 */
public class PlayerCharacter extends Character {
//    UserSettings userSettings;

    public PlayerCharacter(CharacterManager characterManager, BulletAppState bulletAppState, InputManager inputManager, AssetManager assetManager, InfluenceItemManager influenceItemManager, Node nodeToAttachTo, Vector3f startlocation, UserSettings userSettings, Integer joystickDevice, Camera cam, TerrainQuad terrain, InGameObjectTemplateManager inGameObjectTemplateManager, UUID id) {
        super(characterManager, bulletAppState, assetManager, influenceItemManager, nodeToAttachTo, startlocation, "player", inGameObjectTemplateManager, id);

        spawningItems.add(inGameObjectTemplateManager.getSpawingItem("automatic-rifle"));
        spawningItems.add(inGameObjectTemplateManager.getSpawingItem("bolt-action-rifle"));

        for (int j = 0; j < influenceItems.size(); j++) {
            //System.out.println(" j " + j + " influenceitem " + influenceItems.get(j).getName());

            if (influenceItems.get(j).getName().equals("grenade")) {
                influenceItems.get(j).setAmount(10);
            }
            if (influenceItems.get(j).getName().equals("healthpack")) {
                influenceItems.get(j).setAmount(5);
            }
            if (influenceItems.get(j).getName().equals("tranq-bullet")) {
                influenceItems.get(j).setAmount(50);
            }
        }

        /* make a target */
        Box box = new Box(2f, 0.1f, 2f);
        Spatial target = new Geometry("target", box);
//        BoundingBox bb = new BoundingBox(new Vector3f(1f, 0.05f, 1f), 2f, 0.1f, 2f);
//        target.setModelBound(bb);
//       
        Material targetMaterial = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        targetMaterial.setTexture("ColorMap", assetManager.loadTexture("Textures/target.png"));

        target.setMaterial(targetMaterial);

        characterNode.attachChild(target);

        Node soundEmitter = new Node("soundemitter");
        characterNode.attachChild(soundEmitter);

        PlayerControl control = new PlayerControl(influenceItemManager, inputManager, target, userSettings, joystickDevice, cam, terrain, spawningItems, influenceItems, inGameObjectTemplateManager, assetManager, soundEmitter);
        characterNode.addControl(control);

        System.out.println(" Node " + characterNode + " has:");

        List<Spatial> foo = characterNode.getChildren();

        Iterator fooit = foo.iterator();

        while (fooit.hasNext()) {
            System.out.println("  --" + fooit.next());
        }
    }

    public void setLeftRight(Float value) {
        characterNode.getControl(PlayerControl.class).analogLeftRight = value;
    }

    public void setUpDown(Float value) {
        characterNode.getControl(PlayerControl.class).analogUpDown = value;
    }

    public void setTargetLeftRight(Float value) {
        // we don't want 0 because that returns the target to the 0 position and resists normalisation
        // and the target doesn't behave like hotline miami
        characterNode.getControl(PlayerControl.class).analogTargetLeftRight = (value == 0f) ? characterNode.getControl(PlayerControl.class).analogTargetLeftRight : value;
    }

    public void setTargetUpDown(Float value) {
        characterNode.getControl(PlayerControl.class).analogTargetUpDown = (value == 0f) ? characterNode.getControl(PlayerControl.class).analogTargetUpDown : value;
    }

    public void setUseCurrentSpawnItem(Boolean isPressed) {
        characterNode.getControl(PlayerControl.class).useCurrentSpawnItem = isPressed;
    }

    public void setCycleInfluenceItems(Boolean isPressed) {
        characterNode.getControl(PlayerControl.class).cycleInfluenceItems = isPressed;
    }

    public void setCycleSpawnItems(Boolean isPressed) {
        characterNode.getControl(PlayerControl.class).cycleSpawnItems = isPressed;
    }

    public void setJump(Boolean isPressed) {
        characterNode.getControl(PlayerControl.class).jump = isPressed;
    }

    public void setCrouch(Boolean isPressed) {
        characterNode.getControl(PlayerControl.class).crouch = isPressed;
    }

    void setMomentaryZoom(boolean value) {
        characterNode.getControl(PlayerControl.class).isMomentaryZoom = value;
    }

    void setMouseOrGamepadOrKeyboard(String value) {
        characterNode.getControl(PlayerControl.class).setMouseOrGamepadOrKeyboard(value);
    }

    public SpawningItemInventory getSpawnItem() {
        return characterNode.getControl(PlayerControl.class).getSpawnItem();
    }

    public InfluenceItemInventory getInfluenceItem() {
        return characterNode.getControl(PlayerControl.class).getInfluenceItem();
    }

    public Boolean getInCar() {
        return characterNode.getControl(PlayerControl.class).getInCar();
    }

    public void setInCar(Boolean inCar) {
        characterNode.getControl(PlayerControl.class).setInCar(inCar);
    }

}
