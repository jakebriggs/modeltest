/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.asset.AssetManager;
import com.jme3.bullet.BulletAppState;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.system.AppSettings;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.UUID;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;

/**
 *
 * @author Jacob
 */
public class CharacterManager {

    private Node npcsNode;
    private BulletAppState bulletAppState;
    private AssetManager assetManager;
    private InfluenceItemManager influenceItemManager;
    private InGameObjectTemplateManager inGameObjectTemplateManager;
    private AppSettings npcsettings;
    private String settingsfile = "InitialGameState/NPCs.xml";

    private ArrayList<NPCCharacter> npcs = new ArrayList<NPCCharacter>();

    public CharacterManager(Node npcNode, BulletAppState bulletAppState, AssetManager assetManager, InfluenceItemManager influenceItemManager, InGameObjectTemplateManager inGameObjectTemplateManager) {
        this.npcsNode = npcNode;
        this.bulletAppState = bulletAppState;
        this.assetManager = assetManager;
        this.influenceItemManager = influenceItemManager;
        this.inGameObjectTemplateManager = inGameObjectTemplateManager;

        this.settingsfile = settingsfile;
    }

    public void load() {
        Document NPCFile = (Document) assetManager.loadAsset("InitialGameState/NPCs.xml");
        processNPCs(NPCFile.getDocumentElement());

        System.out.println("CharacterManager LOADED");
    }

    public void think() {

    }

    public void spawn(Vector3f location, String faction, String type) {
        npcs.add(new NPCCharacter(this,bulletAppState, assetManager, influenceItemManager, npcsNode, location, faction, type, inGameObjectTemplateManager, UUID.randomUUID()));
    }

    /* recurse throught he DOM */
    public void processNPCs(org.w3c.dom.Node node) {
        // do something with the current node instead of System.out
        System.out.println(node.getNodeName());

        if (node.getNodeName() == "npc") {
            NamedNodeMap attributes = node.getAttributes();

            Float x = Float.parseFloat(attributes.getNamedItem("x").getNodeValue());
            Float y = Float.parseFloat(attributes.getNamedItem("y").getNodeValue());
            Float z = Float.parseFloat(attributes.getNamedItem("z").getNodeValue());
            String faction = attributes.getNamedItem("faction").getNodeValue();
            String type = attributes.getNamedItem("type").getNodeValue();

            npcs.add(new NPCCharacter(this,bulletAppState, assetManager, influenceItemManager, npcsNode, new Vector3f(x, y, z), faction, type, inGameObjectTemplateManager, UUID.randomUUID()));
        }

        NodeList nodeList = node.getChildNodes();
        for (int i = 0; i < nodeList.getLength(); i++) {
            org.w3c.dom.Node currentNode = nodeList.item(i);
            if (currentNode.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
                //calls this method for all the children which is Element
                processNPCs(currentNode);
            }
        }
    }

    public Character findCharacter(UUID id) {
        Iterator it = npcs.iterator();
        
        while (it.hasNext()) {
            NPCCharacter next = (NPCCharacter) it.next();

            if (next.getId().equals(id)) {
                return next;
            }
        }
        
        return null;
    }

    public void manyCharactersInfluenced(HashSet<UUID> ids, InfluenceInventory influence) {
        Iterator it = ids.iterator();

        while (it.hasNext()) {
            characterInfluenced((UUID) it.next(), influence);
        }
    }

    public void characterInfluenced(UUID id, InfluenceInventory influence) {
        /* very lazy way to find influenced character */
        //System.out.println("characterInfluenced " + id);

        findCharacter(id).causeInfluence(influence);
//        Iterator it = npcs.iterator();
//
//        while (it.hasNext()) {
//            NPCCharacter next = (NPCCharacter) it.next();
//
//            if (next.getId().equals(id)) {
//                next.causeInfluence(influence);
//            }
//        }
    }

}
