/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

/**
 *
 * @author Jacob
 */
public class InfluenceInventory {
  
    String name;
    String effect;
    Float effectAmount;
    Float tpf; /* time per frame should attenuate the damage, so we can do "damage per second", rather than "damage per frame". This value should get updated every frame. */
    Float effectRange;
    Float effectLife;
    String method;
    
    public InfluenceInventory() {

    }

    public InfluenceInventory(String name, String effect, Float effectAmount, Float effectRange, Float effectLife, String method) {
        //System.out.println("New InfluenceInventory " + name + " ");
        this.name = name;
        this.effect = effect;
        this.effectAmount = effectAmount;
        this.effectRange = effectRange;
        this.effectLife = effectLife;
        this.method = method;
        
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getEffectAmount() {
        return effectAmount;
    }

    public void setEffectAmount(Float effectAmount) {
        this.effectAmount = effectAmount;
    }

    public Float getEffectRange() {
        return effectRange;
    }

    public void setEffectRange(Float effectRange) {
        this.effectRange = effectRange;
    }

    public Float getEffectLife() {
        return effectLife;
    }

    public void setEffectLife(Float effectLife) {
        this.effectLife = effectLife;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getEffect() {
        return effect;
    }

    public void setEffect(String effect) {
        this.effect = effect;
    }
    
    public Float getTpf() {
        /* if effectlife is zero, do full damage immediately */
        if(effectLife == 0f){
            return 1f;
        } else {
            return tpf;
        }
    }

    public void setTpf(Float tpf) {
        this.tpf = tpf;
    }

}
