package mygame;

import com.jme3.bullet.control.BetterCharacterControl;
import com.jme3.export.JmeExporter;
import com.jme3.export.JmeImporter;
import com.jme3.export.Savable;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;
import com.jme3.scene.control.Control;
import java.io.IOException;
import java.util.UUID;

public class NPCCharacterControl extends AbstractControl implements Savable, Cloneable {

    private Integer updateNum = 0;
    private Integer updateLimit = 1000;
    CharacterManager characterManager;
    UUID id;

    public NPCCharacterControl(CharacterManager characterManager, UUID id) {
        this.characterManager = characterManager;
        this.id = id;
    }

    /**
     * This method is called when the control is added to the spatial, and when
     * the control is removed from the spatial (setting a null value). It can be
     * used for both initialization and cleanup.
     */
    @Override
    public void setSpatial(Spatial spatial) {
        super.setSpatial(spatial);
        /* Example:
         if (spatial != null){
         // initialize
         }else{
         // cleanup
         }
         */
    }

    /**
     * Implement your spatial's behaviour here. From here you can modify the
     * scene graph and the spatial (transform them, get and set userdata, etc).
     * This loop controls the spatial while the Control is enabled.
     *
     * @param tpf
     */
    @Override
    protected void controlUpdate(float tpf) {
        if (spatial != null) {
            /* check health and stamina - if none, fall down */
            Character myCharacter = characterManager.findCharacter(id);
            BetterCharacterControl bcc = spatial.getControl(BetterCharacterControl.class);

            if (bcc != null) {
                if (myCharacter.getDamage() <= 0) {
                    //System.out.println("NPCCharacterControl says DEAD " + id);
                     bcc.setWalkDirection(Vector3f.ZERO);
                } else if (myCharacter.getStamina() <= 0) {
                    //System.out.println("NPCCharacterControl says KNOCKEDOUT " + id);
                    bcc.setWalkDirection(Vector3f.ZERO);
                } else {

                    if (updateNum > updateLimit) {
                        updateNum = 0;
                    }

                    if (updateNum == 0) {
                        Double x = ((Math.random() * 2) - 1) * 10;
                        Double z = ((Math.random() * 2) - 1) * 10;

                        bcc.setWalkDirection(new Vector3f(x.floatValue(), 0.0f, z.floatValue()));
                    }

                    //this.setTargetLocalTranslation(this.getWalkDirection());
                    updateNum++;
                }
            }
        }
    }

    @Override
    public Control cloneForSpatial(Spatial spatial) {
        final NPCCharacterControl control = new NPCCharacterControl(characterManager, id);
        /* Optional: use setters to copy userdata into the cloned control */
        // control.setIndex(i); // example
        control.setSpatial(spatial);
        return control;
    }

    @Override
    protected void controlRender(RenderManager rm, ViewPort vp) {
        /* Optional: rendering manipulation (for advanced users) */
    }

    @Override
    public void read(JmeImporter im) throws IOException {
        super.read(im);
        // im.getCapsule(this).read(...);
    }

    @Override
    public void write(JmeExporter ex) throws IOException {
        super.write(ex);
        // ex.getCapsule(this).write(...);
    }

}
