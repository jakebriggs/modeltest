/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.asset.AssetManager;
import com.jme3.bullet.BulletAppState;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import java.util.ArrayList;
import java.util.Hashtable;

/**
 *
 * @author Jacob
 */
public class InfluenceItemManager {

    private Node influenceItemNode;
    private BulletAppState bulletAppState;
    private AssetManager assetManager;
    private InfluenceManager influenceManager;
    private ArrayList<InfluenceItem> influenceItems = new ArrayList<InfluenceItem>();
    private Hashtable influenceTypes;

    public InfluenceItemManager(Node nodeToAttachTo, BulletAppState bulletAppState, AssetManager assetManager, InfluenceManager influenceManager) {
        this.influenceItemNode = nodeToAttachTo;
        this.bulletAppState = bulletAppState;
        this.assetManager = assetManager;
        this.influenceManager = influenceManager;
        
        influenceTypes = new Hashtable();
    }

    public void load() {
        System.out.println("InfluenceItemManager LOADED");
    }

//    public void spawn(Vector3f location, Vector3f direction) {
//        this.spawn(location, direction, "grenade0");
//    }
    public void spawn(Node nodeToAttachTo, Vector3f direction, InfluenceItemInventory influenceItemInventory) {
        //System.out.println("#### Adding an item attached to " + nodeToAttachTo.getName() + " in this direction " + direction + " of this type " + influenceItemInventory.getName());

        influenceItems.add(new InfluenceItem(bulletAppState, assetManager, influenceManager, nodeToAttachTo, direction, influenceItemInventory));
    }

    public void spawn(Vector3f location, Vector3f direction, InfluenceItemInventory influenceItemInventory) {
        //System.out.println("Adding an item from " + location + " in this direction " + direction + " influenceItems.size() " + influenceItems.size());
        influenceItems.add(new InfluenceItem(bulletAppState, assetManager, influenceManager, influenceItemNode, direction, influenceItemInventory));
    }
}
