package mygame;

import com.jme3.app.SimpleApplication;
import com.jme3.bounding.BoundingBox;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.PhysicsSpace;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.material.Material;
import com.jme3.math.Vector3f;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.AnalogListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.light.DirectionalLight;
import com.jme3.scene.Node;
import com.jme3.terrain.geomipmap.TerrainQuad;
import com.jme3.terrain.heightmap.AbstractHeightMap;
import com.jme3.terrain.heightmap.ImageBasedHeightMap;
import com.jme3.texture.Texture;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.bullet.control.VehicleControl;
import com.jme3.bullet.objects.VehicleWheel;
import com.jme3.bullet.util.CollisionShapeFactory;
import com.jme3.export.xml.XMLImporter;
import com.jme3.font.BitmapText;
import com.jme3.input.Joystick;
import com.jme3.input.controls.JoyButtonTrigger;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Matrix3f;
import com.jme3.renderer.queue.RenderQueue.ShadowMode;
import com.jme3.scene.Geometry;
import com.jme3.scene.Spatial;
import com.jme3.shadow.DirectionalLightShadowRenderer;
import com.jme3.system.AppSettings;
import com.jme3.ui.Picture;
import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Sample 3 - how to load an OBJ model, and OgreXML model, a material/texture,
 * or text.
 */
public class Main extends SimpleApplication {

    private TerrainQuad terrain;
    Material mat_terrain;

    //Temporary vectors used on each frame.
    //They here to avoid instanciating new vectors on each frame
    private Vector3f camOffset = new Vector3f(0.0f, 200.0f, 0.0f);
    //private JmeCursor mousePosition = new JmeCursor();
    //private Spatial sceneModel;
    private BulletAppState bulletAppState;
    private RigidBodyControl landscape;
    private PlayerCharacter playerCharacter;
    private Rideable playerRideable;
    private CharacterManager characterManager;
    private InfluenceItemManager influenceItemManager;
    private InfluenceManager influenceManager;
    private InGameObjectTemplateManager inGameObjectTemplateManager;
    private StaticModelManager staticModelManager;
    private RideableManager rideableManager;
    private UserSettings userSettings;
    private String gameName = "mygame";
    private Node npcs = new Node("npcs");
    private Node staticModels = new Node("staticModels"); /* buildings and tress and shit */
    private Node rideables = new Node("rideables");
    private Node influenceItems = new Node("influenceItems");
    private Node influences = new Node("influences");

    private Spatial target;
    private Joystick[] joysticks;
    private Integer joystickDevice = null;
    private String joystickDeviceName = null;
    private BitmapText hudText;

    /* vehicle test */
//    private VehicleControl vehicleControl;
//    private VehicleWheel fr, fl, br, bl;
//    private Node node_fr, node_fl, node_br, node_bl;
//    private float wheelRadius;
//    private float steeringValue = 0;
//    private float accelerationValue = 0;
//    private Node carNode;
    /* vehicle test */

    public static void main(String[] args) {
        Main app = new Main();

        AppSettings settings = new AppSettings(true);
        settings.setUseJoysticks(true);
        app.setSettings(settings);

        app.setShowSettings(true);

        app.start();

    }

    @Override
    public void simpleInitApp() {

        assetManager.registerLoader(XMLLoader.class, "xml");
        InGameObjectTemplateManager inGameObjectTemplateManager = new InGameObjectTemplateManager(assetManager);
        inGameObjectTemplateManager.load();

        /* setting's and stuff ================================================================ */
        try {
            userSettings = new UserSettings(settings, gameName);

            userSettings.load();

            userSettings.save();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }

        /* ================================================================ */
        bulletAppState = new BulletAppState();
        bulletAppState.setDebugEnabled(true);
        stateManager.attach(bulletAppState);

        mat_terrain = new Material(assetManager, "Common/MatDefs/Terrain/Terrain.j3md");
        mat_terrain.setTexture("Alpha", assetManager.loadTexture("Textures/alphamap.png"));

        /**
         * 1.4) Add ROAD texture into the blue layer (Tex3)
         */
        Texture rock = assetManager.loadTexture("Textures/grid8.png");
        //Texture rock = assetManager.loadTexture("Textures/dirtbig.jpg");
        //Texture rock = assetManager.loadTexture("Textures/dirt3.png");
        rock.setWrap(Texture.WrapMode.Repeat);
        mat_terrain.setTexture("Tex3", rock);
        mat_terrain.setFloat("Tex3Scale", 2048f);

        /**
         * 2. Create the height map
         */
        Texture heightMapImage = assetManager.loadTexture("Textures/wellington-1m-dem 4096.png");

        AbstractHeightMap heightmap = new ImageBasedHeightMap(heightMapImage.getImage());
        heightmap.load();
        heightmap.smooth(1f);

        /**
         * 3. We have prepared material and heightmap. Now we create the actual
         * terrain: 3.1) Create a TerrainQuad and name it "my terrain". 3.2) A
         * good value for terrain tiles is 64x64 -- so we supply 64+1=65. 3.3)
         * We prepared a heightmap of size 512x512 -- so we supply 512+1=513.
         * 3.4) As LOD step scale we supply Vector3f(1,1,1). 3.5) We supply the
         * prepared heightmap itself.
         */
        int patchSize = 65;
        terrain = new TerrainQuad("my terrain", patchSize, 4097, heightmap.getHeightMap());

        /**
         * 4. We give the terrain its material, position & scale it, and attach
         * it.
         */
        terrain.setMaterial(mat_terrain);
        terrain.setLocalTranslation(0, -10, 0);
        terrain.setLocalScale(1f, 1f, 1f);
        
        rootNode.attachChild(terrain);

        /* player character ================================================================ */
        /* throwables ================================================================ */
        influenceManager = new InfluenceManager(influences, bulletAppState, assetManager, npcs, inGameObjectTemplateManager);
        //influenceManager = new InfluenceManager(influences, bulletAppState, assetManager, rootNode, inGameObjectTemplateManager);
        influenceManager.load();

        influenceItemManager = new InfluenceItemManager(influenceItems, bulletAppState, assetManager, influenceManager);
        influenceItemManager.load();

        /* the target ===============================================================*/
//        Box box = new Box(2f, 0.1f, 2f);
//        target = new Geometry("target", box);
//
//        Material targetMaterial = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
//        targetMaterial.setTexture("ColorMap", assetManager.loadTexture("Textures/target.png"));
//
//        target.setMaterial(targetMaterial);
//        
        /* npc and player character ================================================================ */
        characterManager = new CharacterManager(npcs, bulletAppState, assetManager, influenceItemManager, inGameObjectTemplateManager);
        characterManager.load();
        this.influenceManager.setCharacterManager(characterManager);

        playerCharacter = new PlayerCharacter(characterManager, bulletAppState, inputManager, assetManager, influenceItemManager, rootNode, new Vector3f(0, 10, 0), userSettings, joystickDevice, cam, terrain, inGameObjectTemplateManager, UUID.randomUUID());

//        /* effect areas ===============================================================*/
//        
//        effectAreaManager = new EffectAreaManager(effectables, bulletAppState, assetManager);
//        
        staticModelManager = new StaticModelManager(staticModels, bulletAppState, assetManager, inGameObjectTemplateManager);
        staticModelManager.load();

        /* rideable manager */
        rideableManager = new RideableManager(rideables, bulletAppState, assetManager, inputManager, userSettings, joystickDevice, inGameObjectTemplateManager);
        rideableManager.load();
        
        rootNode.attachChild(npcs);
        rootNode.attachChild(influenceItems);
        rootNode.attachChild(influences);
        rootNode.attachChild(staticModels);
        rootNode.attachChild(rideables);
        
        /* ================================================================ */

        /* make terrain solid */
        CollisionShape sceneShape = CollisionShapeFactory.createMeshShape((Node) terrain);
        landscape = new RigidBodyControl(sceneShape, 0);
        terrain.addControl(landscape);

        bulletAppState.getPhysicsSpace().add(landscape);
        //bulletAppState.getPhysicsSpace().add(playerControl);

        // You must add a light to make the model visible
        DirectionalLight sun = new DirectionalLight();
        sun.setDirection(new Vector3f(-0.5f, -0.5f, -0.5f));
        rootNode.addLight(sun);
        rootNode.setShadowMode(ShadowMode.CastAndReceive);

        final int SHADOWMAP_SIZE = 1024;
        DirectionalLightShadowRenderer dlsr = new DirectionalLightShadowRenderer(assetManager, SHADOWMAP_SIZE, 3);
        dlsr.setLight(sun);
        viewPort.addProcessor(dlsr);

        flyCam.setEnabled(false);
//        chaseCam = new ChaseCamera(cam, playerCharacter.characterNode, inputManager);
//        
//        chaseCam.setLookAtOffset(new Vector3f(0, 20, 0));
//        chaseCam.setMaxDistance(80);
//        System.out.println("maxdistance " + chaseCam.getMaxDistance());

        inputManager.setCursorVisible(false);

        cam.setLocation(playerCharacter.getLocalTranslation().add(camOffset));
        cam.lookAt(playerCharacter.getLocalTranslation(), Vector3f.UNIT_Y);

        /* initialise key bindings, and joysticks */
        initKeys(); // load my custom keybinding

        hudText = new BitmapText(guiFont, false);
        hudText.setSize(guiFont.getCharSet().getRenderedSize());      // font size
        hudText.setColor(ColorRGBA.Black);                             // font color
        hudText.setLocalTranslation(0, settings.getHeight(), 0); // position

        Picture pic = new Picture("HUD Picture");
        pic.setImage(assetManager, "Textures/whitedot.png", true);
        pic.setWidth(settings.getWidth());
        pic.setHeight(hudText.getLineHeight());
        pic.setPosition(0, settings.getHeight() - hudText.getLineHeight());

        guiNode.attachChild(pic);
        guiNode.attachChild(hudText);
//
//        buildVehicle();
//        carNode.setLocalTranslation(Vector3f.ZERO);

//        System.out.println(mineNode(rootNode));
//
//        try {
//            File f = File.createTempFile("node", ".html");
//            FileWriter fw = new FileWriter(f);
//            fw.write(mineNode(rootNode));
//            fw.flush();
//            fw.close();
//            Desktop.getDesktop().open(f);
//
//        } catch (IOException ex) {
//            Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE, "Error writing html file", ex);
//        }

    }

    /**
     * This is the main event loop--walking happens here. We check in which
     * direction the player is walking by interpreting the camera direction
     * forward (camDir) and to the side (camLeft). The setWalkDirection()
     * command is what lets a physics-controlled player walk. We also make sure
     * here that the camera moves with player.
     */
    @Override
    public void simpleUpdate(float tpf) {

        if(playerCharacter.getInCar()){
            cam.setLocation(playerRideable.getLocalTranslation().add(camOffset));
            cam.lookAt(playerRideable.getLocalTranslation(), Vector3f.UNIT_Y);
        } else {
            cam.setLocation(playerCharacter.getLocalTranslation().add(camOffset));
            cam.lookAt(playerCharacter.getLocalTranslation(), Vector3f.UNIT_Y);
        }
        Float damage = playerCharacter.getDamage();
        Float stamina = playerCharacter.getStamina();
        SpawningItemInventory spawnItemInventory = playerCharacter.getSpawnItem();
        InfluenceItemInventory influenceItemInventory = playerCharacter.getInfluenceItem();

        hudText.setText("Damage |" + damage + "| Stamina |" + stamina + "| Weapon |" + spawnItemInventory.getName() + "| Ammo |" + influenceItemInventory.getAmount() + " " + influenceItemInventory.getName() + "|");

        //cam.contains(playerCharacter.characterNode.getChild("target"));
    }

    /**
     * Custom Keybinding: Map named actions to inputs.
     */
    private void initKeys() {
        System.out.println("initKeys |" + userSettings.getString("defaultJoystickName") + "|");

        joysticks = inputManager.getJoysticks();

        // You can map one or several inputs to one named actions
        inputManager.addMapping("Pause", new KeyTrigger(userSettings.getInteger("keyboardMapPause")));
        inputManager.addMapping("SpawnNPC", new KeyTrigger(userSettings.getInteger("keyboardMapSpawnNPC")));
        inputManager.addMapping("ZoomIn", new KeyTrigger(userSettings.getInteger("keyboardMapZoomIn")));
        inputManager.addMapping("ZoomOut", new KeyTrigger(userSettings.getInteger("keyboardMapZoomOut")));
        inputManager.addMapping("MomentaryZoom", new KeyTrigger(userSettings.getInteger("keyboardMapMomentaryZoom")));
        inputManager.addMapping("CarToggle", new KeyTrigger(userSettings.getInteger("keyboardMapCarToggle")));

//        inputManager.addMapping("Left", new KeyTrigger(userSettings.getInteger("keyboardMapLeft")));
//        inputManager.addMapping("Right", new KeyTrigger(userSettings.getInteger("keyboardMapRight")));
//        inputManager.addMapping("Up", new KeyTrigger(userSettings.getInteger("keyboardMapUp")));
//        inputManager.addMapping("Down", new KeyTrigger(userSettings.getInteger("keyboardMapDown")));        
//        
        if (joysticks == null || joysticks.length == 0) {
            System.out.println("Cannot find any joysticks!" + settings.useJoysticks());
        } else {
            System.out.println("Found at least one joystick...." + settings.useJoysticks());

            //is the default joystick attached?
            for (int i = 0; i < joysticks.length; i++) {
                System.out.println("  --  " + joysticks[i].getName() + "| id |" + joysticks[i].getJoyId() + "|");

                if (userSettings.getString("defaultJoystickName").equals(joysticks[i].getName())) {

                    System.out.println("    -- found default, its id is " + joysticks[i].getJoyId());

                    joystickDevice = joysticks[i].getJoyId();
                    joystickDeviceName = joysticks[i].getName();
                }
            }

            //If not, then just grab the first joystick because why not
            if (joystickDevice == null) {
                joystickDevice = 0;
                joystickDeviceName = joysticks[0].getName();
            }

            System.out.println("Gonna use joystick device " + joystickDevice + " with name " + joystickDeviceName);

            /* maybe have a raw listener instead of the joyActionTrigger because the axis values are much easier to get at */
            inputManager.addRawInputListener(new JoystickEventListener(playerCharacter, joystickDevice, settings));

            inputManager.addMapping("Pause", new JoyButtonTrigger(joystickDevice, userSettings.getInteger("joyMapPause")));
            inputManager.addMapping("ZoomIn", new JoyButtonTrigger(joystickDevice, userSettings.getInteger("joyMapZoomIn")));
            inputManager.addMapping("ZoomOut", new JoyButtonTrigger(joystickDevice, userSettings.getInteger("joyMapZoomOut")));
            inputManager.addMapping("MomentaryZoom", new JoyButtonTrigger(joystickDevice, userSettings.getInteger("joyMapMomentaryZoom")));
            inputManager.addMapping("SpawnNPC", new JoyButtonTrigger(joystickDevice, userSettings.getInteger("joyMapSpawnNPC")));

        }

        inputManager.addListener(actionListener, "Pause");
        inputManager.addListener(actionListener, "ZoomIn");
        inputManager.addListener(actionListener, "ZoomOut");
        inputManager.addListener(actionListener, "MomentaryZoom");
        inputManager.addListener(actionListener, "SpawnNPC");
        inputManager.addListener(actionListener, "CarToggle");
//        
//        inputManager.addListener(actionListener, "Left");
//        inputManager.addListener(actionListener, "Right");
//        inputManager.addListener(actionListener, "Up");
//        inputManager.addListener(actionListener, "Down");

    }

    private static String mineNode(Node parent) {

        String ptype = parent.getClass().getSimpleName();

        String s = parent.getName() + " (" + ptype + ")" + "<br><ul>";

        if (parent.getChildren().isEmpty()) {
            return parent.getName() + " (" + ptype + ")";
        } else {

            for (Spatial child : parent.getChildren()) {

                if (child instanceof Node) {

                    s = s + "<li>" + mineNode((Node) child) + "</li>";

                } else {

                    String type = child.getClass().getSimpleName();

                    s = s + "<li>" + child.getName() + " (" + type + ")" + "</li>";

                }

            }

        }

        s = s + "</ul>";

        return s;
    }

    private ActionListener actionListener = new ActionListener() {
        public void onAction(String name, boolean keyPressed, float tpf) {
            //System.out.println("Key pressed onAction name |" + name + "| keyPressed | " + keyPressed + "|");

            if (name.equals("ZoomIn")) {
                camOffset.y = camOffset.y - 10;
            } else if (name.equals("ZoomOut")) {
                camOffset.y = camOffset.y + 10;
            } else if (name.equals("MomentaryZoom")) {
                playerCharacter.setMomentaryZoom(keyPressed);
                if (keyPressed) {
                    camOffset.y = camOffset.y + 100;
                } else {
                    camOffset.y = camOffset.y - 100;
                }
            } else if (name.equals("SpawnNPC") && keyPressed) {
                characterManager.spawn(new Vector3f(3, 10, 3), "none", "generic");
            } else if (name.equals("Pause")) {
                System.out.println("========================== PAUSE ==============================");
            } else if (name.equals("CarToggle") && keyPressed) {
                /* this is pretty dumb, but lets do it like this for testing and stuff */
                playerCharacter.setInCar(!playerCharacter.getInCar());
                playerRideable = rideableManager.getClosestRideable();
                playerRideable.setBeingRidden(!playerRideable.getBeingRidden());
            }
        }
    };
    private AnalogListener analogListener = new AnalogListener() {
        public void onAnalog(String name, float value, float tpf) {

            //System.out.println("onAnalog name |" + name + "| value |" + value + "| tpf |" + tpf + "|" + inputManager.getCursorPosition());
        }
    };

//    /* test vehicle */
//    private PhysicsSpace getPhysicsSpace() {
//        return bulletAppState.getPhysicsSpace();
//    }
//
//    private Geometry findGeom(Spatial spatial, String name) {
//        if (spatial instanceof Node) {
//            Node node = (Node) spatial;
//            for (int i = 0; i < node.getQuantity(); i++) {
//                Spatial child = node.getChild(i);
//                Geometry result = findGeom(child, name);
//                if (result != null) {
//                    return result;
//                }
//            }
//        } else if (spatial instanceof Geometry) {
//            if (spatial.getName().startsWith(name)) {
//                return (Geometry) spatial;
//            }
//        }
//        return null;
//    }
//
//    private void buildVehicle() {
//        float stiffness = 120.0f;//200=f1 car
//        float compValue = 0.2f; //(lower than damp!)
//        float dampValue = 0.3f;
//        final float mass = 800;
//
//        //Load model and get chassis Geometry
//        carNode = (Node) assetManager.loadModel("Models/Ferrari/Car.scene");
//        carNode.setShadowMode(ShadowMode.Cast);
//        Geometry chasis = findGeom(carNode, "Car");
//        BoundingBox box = (BoundingBox) chasis.getModelBound();
//
//        //Create a hull collision shape for the chassis
//        CollisionShape carHull = CollisionShapeFactory.createDynamicMeshShape(chasis);
//        //carHull.setScale(new Vector3f(3, 3, 3));
//
//        //Create a vehicle control
//        vehicleControl = new VehicleControl(carHull, mass);
//        carNode.addControl(vehicleControl);
//
//        //Setting default values for wheels
//        vehicleControl.setSuspensionCompression(compValue * 2.0f * FastMath.sqrt(stiffness));
//        vehicleControl.setSuspensionDamping(dampValue * 2.0f * FastMath.sqrt(stiffness));
//        vehicleControl.setSuspensionStiffness(stiffness);
//        vehicleControl.setMaxSuspensionForce(10000);
//
//        //Create four wheels and add them at their locations
//        //note that our fancy car actually goes backwards..
//        Vector3f wheelDirection = new Vector3f(0, -1, 0);
//        Vector3f wheelAxle = new Vector3f(-1, 0, 0);
//
//        Geometry wheel_fr = findGeom(carNode, "WheelFrontRight");
//        wheel_fr.center();
//        box = (BoundingBox) wheel_fr.getModelBound();
//        wheelRadius = box.getYExtent();
//        float back_wheel_h = (wheelRadius * 1.7f) - 1f;
//        float front_wheel_h = (wheelRadius * 1.9f) - 1f;
//        vehicleControl.addWheel(wheel_fr.getParent(), box.getCenter().add(0, -front_wheel_h, 0),
//                wheelDirection, wheelAxle, 0.2f, wheelRadius, true);
//
//        Geometry wheel_fl = findGeom(carNode, "WheelFrontLeft");
//        wheel_fl.center();
//        box = (BoundingBox) wheel_fl.getModelBound();
//        vehicleControl.addWheel(wheel_fl.getParent(), box.getCenter().add(0, -front_wheel_h, 0),
//                wheelDirection, wheelAxle, 0.2f, wheelRadius, true);
//
//        Geometry wheel_br = findGeom(carNode, "WheelBackRight");
//        wheel_br.center();
//        box = (BoundingBox) wheel_br.getModelBound();
//        vehicleControl.addWheel(wheel_br.getParent(), box.getCenter().add(0, -back_wheel_h, 0),
//                wheelDirection, wheelAxle, 0.2f, wheelRadius, false);
//
//        Geometry wheel_bl = findGeom(carNode, "WheelBackLeft");
//        wheel_bl.center();
//        box = (BoundingBox) wheel_bl.getModelBound();
//        vehicleControl.addWheel(wheel_bl.getParent(), box.getCenter().add(0, -back_wheel_h, 0),
//                wheelDirection, wheelAxle, 0.2f, wheelRadius, false);
//
//        vehicleControl.getWheel(2).setFrictionSlip(4);
//        vehicleControl.getWheel(3).setFrictionSlip(4);
//
//        rootNode.attachChild(carNode);
//        getPhysicsSpace().add(vehicleControl);
//    }

}
