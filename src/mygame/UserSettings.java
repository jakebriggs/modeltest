/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.input.JoyInput;
import com.jme3.input.KeyInput;
import com.jme3.input.MouseInput;
import com.jme3.input.controls.JoyAxisTrigger;
import com.jme3.input.controls.MouseButtonTrigger;
import com.jme3.system.AppSettings;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 *
 * @author Jacob
 */
public class UserSettings {

    private AppSettings settings;
    private File settingsFile;
    private String gameName;
    private String settingsFilename = "settings.properties";

    public UserSettings(AppSettings settings, String gameName) throws IOException {
        this.settings = settings;
        this.gameName = gameName;

        //System.out.println("user home dir is " + System.getProperty("user.home"));
        File homedir = new File(System.getProperty("user.home"));
        File gamedir = new File(System.getProperty("user.home") + "/" + this.gameName);
        this.settingsFile = new File(System.getProperty("user.home") + "/" + this.gameName + "/" + this.settingsFilename);

        if (homedir.isDirectory()) {
            if (!gamedir.isDirectory()) {
                gamedir.mkdir();
            }

            if (!this.settingsFile.exists()) {
                this.settingsFile.createNewFile();
                this.setDefaults();
                this.save();
            }
        } else {
            System.out.println("user home dir is not a directory " + System.getProperty("user.home"));
        }

    }

    private void setDefaults() {
        settings.putString("defaultJoystickName", "Controller (XBOX 360 For Windows)");

        settings.putInteger("keyboardMapCarToggle", KeyInput.KEY_M);

        settings.putInteger("keyboardMapLeft", KeyInput.KEY_A);
        settings.putInteger("keyboardMapRight", KeyInput.KEY_D);
        settings.putInteger("keyboardMapUp", KeyInput.KEY_W);
        settings.putInteger("keyboardMapDown", KeyInput.KEY_S);

        settings.putInteger("keyboardMapTargetLeft", KeyInput.KEY_J);
        settings.putInteger("keyboardMapTargetRight", KeyInput.KEY_L);
        settings.putInteger("keyboardMapTargetUp", KeyInput.KEY_I);
        settings.putInteger("keyboardMapTargetDown", KeyInput.KEY_K);

        settings.putInteger("keyboardMapJump", KeyInput.KEY_SPACE);
        settings.putInteger("keyboardMapCycleSpawnItems", KeyInput.KEY_R);
        settings.putInteger("keyboardMapCycleInfluenceItems", KeyInput.KEY_T);
        settings.putInteger("keyboardMapPause", KeyInput.KEY_P);
        settings.putInteger("keyboardMapSpawnNPC", KeyInput.KEY_V);
        settings.putInteger("keyboardMapUseCurrentSpawnItem", KeyInput.KEY_U);

        settings.putInteger("keyboardMapZoomIn", KeyInput.KEY_UP);
        settings.putInteger("keyboardMapZoomOut", KeyInput.KEY_DOWN);
        settings.putInteger("keyboardMapMomentaryZoom", KeyInput.KEY_X);

        settings.putInteger("mouseMapUseCurrentSpawnItem", MouseInput.BUTTON_LEFT);
        settings.putInteger("mouseMapCycleInfluenceItems", MouseInput.BUTTON_RIGHT);

        settings.putInteger("joystickMapLeftRight", 0);
        settings.putInteger("joystickMapUpDown", 1);
        settings.putInteger("joystickMapTargetLeftRight", 2);
        settings.putInteger("joystickMapTargetUpDown", 3);

        settings.putInteger("joyMapUseCurrentSpawnItem", 0);
        settings.putInteger("joyMapCycleInfluenceItems", 1);
        settings.putInteger("joyMapJump", 2);
        settings.putInteger("joyMapCycleSpawnItems", 3);
        settings.putInteger("joyMapPause", 4);
        settings.putInteger("joyMapSpawnNPC", 5);
        settings.putInteger("joyMapZoomIn", 6);
        settings.putInteger("joyMapZoomOut", 7);
        settings.putInteger("joyMapMomentaryZoom", 8);

        settings.putString("NPCFile", "GameSetup/npcfile.properties");

    }

    public void load() throws FileNotFoundException, IOException {
        FileInputStream saveFileStream = new FileInputStream(this.settingsFile);

        /* load defaults, then overwrite */
        this.setDefaults();
        settings.load(saveFileStream);
        //System.out.println("LOADED");
    }

    public void save() throws FileNotFoundException, IOException {
        FileOutputStream saveFileStream = new FileOutputStream(this.settingsFile);
        settings.save(saveFileStream);

        //System.out.println("SAVED");
    }

    public Boolean setInteger(String name, Integer value) {
        settings.putInteger(name, value);

        return true;
    }

    public Integer getInteger(String name) {
        return settings.getInteger(name);
    }

    public Boolean setString(String name, String value) {
        settings.putString(name, value);

        return true;
    }

    public String getString(String name) {
        return settings.getString(name);
    }

}
