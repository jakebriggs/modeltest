/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.asset.AssetManager;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.collision.shapes.CapsuleCollisionShape;
import com.jme3.bullet.control.CharacterControl;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.shape.Box;

/**
 *
 * @author Jacob
 */
public class OldCharacter extends Geometry {

    public Geometry playerTarget;
    private Vector3f walkDirection = new Vector3f();
    private CharacterControl characterControl;
    public boolean dead = false;

    public OldCharacter(Node characterNode, BulletAppState bulletAppState, AssetManager assetManager, Vector3f startlocation, String name) {
        super(name, new Box(1, 2, 1));

        CapsuleCollisionShape capsuleShape = new CapsuleCollisionShape(1.5f, 2f, 1);
        characterControl = new CharacterControl(capsuleShape, 0.05f);
        characterControl.setJumpSpeed(20);
        characterControl.setFallSpeed(30);
        characterControl.setGravity(30);
        characterControl.setPhysicsLocation(startlocation);

        
        
        // Create a player with a simple texture from test_data
        this.addControl(characterControl);
        Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        mat.setColor("Color", ColorRGBA.Blue);
        this.setMaterial(mat);

        Box t = new Box(1, 2, 1);
        playerTarget = new Geometry("PlayerTarget", t);

        Material matTarget = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        matTarget.setColor("Color", ColorRGBA.Black);
        playerTarget.setMaterial(matTarget);

        characterNode.attachChild(this);
        characterNode.attachChild(playerTarget);
        this.setTargetLocalTranslation(this.getPhysicsLocation());
        bulletAppState.getPhysicsSpace().add(characterControl);
    }

    public Vector3f getPhysicsLocation() {
        return characterControl.getPhysicsLocation();
    }

    public void setWalkDirection(Vector3f walkDirection) {
        characterControl.setWalkDirection(walkDirection);
    }

    public Vector3f getWalkDirection() {
        return characterControl.getWalkDirection();
    }

    public void setTargetLocalTranslation(Vector3f localTranslation) {
        playerTarget.setLocalTranslation(this.getPhysicsLocation().add(localTranslation));

        Quaternion thetarget = new Quaternion();
        thetarget.lookAt(new Vector3f(10,10,10),  new Vector3f(0, 1, 0));
//        thetarget.fromAngleAxis(FastMath.PI, new Vector3f(0, 1, 0));

        //this.lookAt(playerTarget.getLocalTranslation(),Vector3f.UNIT_Y);
        //this.rotate(0,1,0);
    }

    public Vector3f getTargetLocalTranslation() {
        return playerTarget.getLocalTranslation();
    }

    public void jump() {
        characterControl.jump();
    }

    public void shot() {
        //System.out.println("I got shot " + this.getName());
        dead = true;
        this.setWalkDirection(new Vector3f(0, 0, 0));
    }
}
