/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

/**
 *
 * @author Jacob
 */
public class InfluenceItemInventory {

    String name;
    Integer amount;
    String influence;
    String mechanism;
    String model;
    String path;
    Float timeActive;

    public InfluenceItemInventory() {

    }

    public InfluenceItemInventory(String name, Integer amount, String model) {
        //System.out.println("New InfluenceItemInventory " + name + " " + amount);
        this.name = name;
        this.amount = amount;
        this.model = model;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getInfluence() {
        return influence;
    }

    public void setInfluence(String influence) {
        this.influence = influence;
    }

    public String getMechanism() {
        return mechanism;
    }

    public void setMechanism(String mechanism) {
        this.mechanism = mechanism;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Float getTimeActive() {
        return timeActive;
    }

    public void setTimeActive(Float timeActive) {
        this.timeActive = timeActive;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

}
