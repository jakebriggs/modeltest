/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.asset.AssetManager;
import com.jme3.bullet.BulletAppState;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import java.util.ArrayList;
import java.util.Hashtable;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;

/**
 *
 * @author Jacob
 */
public class InfluenceManager {

    private Node influenceNode;
    private Node nodeToInfluence;
    private BulletAppState bulletAppState;
    private AssetManager assetManager;
    private ArrayList<Influence> influenceItems = new ArrayList<Influence>();
    private Hashtable influenceTypes;
    private InGameObjectTemplateManager inGameObjectTemplateManager;
    private CharacterManager characterManager;

    public CharacterManager getCharacterManager() {
        return characterManager;
    }

    public void setCharacterManager(CharacterManager characterManager) {
        this.characterManager = characterManager;
    }
    
    public InfluenceManager(Node nodeToAttachTo, BulletAppState bulletAppState, AssetManager assetManager, Node nodeToInfluence,InGameObjectTemplateManager inGameObjectTemplateManager) {
        this.influenceNode = nodeToAttachTo;
        this.bulletAppState = bulletAppState;
        this.assetManager = assetManager;
        this.nodeToInfluence = nodeToInfluence;
        this.inGameObjectTemplateManager = inGameObjectTemplateManager;
        
        influenceTypes = new Hashtable();
    }

    public void load() {
        System.out.println("InfluenceManager LOADED");
    }

//    public void spawn(Vector3f location, Vector3f direction) {
//        this.spawn(location, direction, "explosion");
//    }
//    public void spawn(Node nodeToAttachTo, Vector3f direction, String type) {
//        influenceItems.add(new Influence(bulletAppState, assetManager, inGameObjectTemplateManager, influenceNode, nodeToInfluence,nodeToAttachTo.getLocalTranslation(), direction,type,characterManager));
//    }
    
    public void spawn(Vector3f location, Vector3f direction, String type) {
        //System.out.println("Adding an item from " + location + " in this direction " + direction + " influenceItems.size() " + influenceItems.size() );
        influenceItems.add(new Influence(bulletAppState, assetManager, inGameObjectTemplateManager, influenceNode, nodeToInfluence, location, direction,type,characterManager));
    }
    
    
}
