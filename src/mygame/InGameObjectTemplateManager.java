/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* loads stuff from items, acts as a repository of items like weapons and influence items so the other managers don't have to load anything */
package mygame;

import com.jme3.asset.AssetManager;
import java.util.ArrayList;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author Jacob
 */
public class InGameObjectTemplateManager {

    private AssetManager assetManager;
    private Document itemsFile;

    public InGameObjectTemplateManager(AssetManager assetManager) {
        this.assetManager = assetManager;

    }

    public void load() {
        itemsFile = (Document) assetManager.loadAsset("InitialGameState/Items.xml");
        debugInGameObjectTemplates(itemsFile.getDocumentElement(), "");

        //System.out.println("InGameObjectTemplateManager LOADED");
    }

    /* get details for a specific spawning item*/
    public SpawningItemInventory getSpawingItem(String spawningItem) {
        Node rootNode = itemsFile.getDocumentElement();
        SpawningItemInventory returnSpawingItem = new SpawningItemInventory();

        NodeList items = rootNode.getChildNodes();

        for (int itemcount = 0; itemcount < items.getLength(); itemcount++) {
            Node anItem = items.item(itemcount);

            if (anItem.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
                //System.out.println("  anItem name " + anItem.getNodeName());

                if (anItem.getNodeName().equals("spawning-items")) {

                    NodeList spawnitems = anItem.getChildNodes();

                    for (int spawnitemscount = 0; spawnitemscount < spawnitems.getLength(); spawnitemscount++) {
                        Node spawnItem = spawnitems.item(spawnitemscount);

                        if (spawnItem.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
                            //System.out.println("    spawnItem name " + spawnItem.getNodeName());

                            if (spawnItem.getNodeName().equals("spawning-item")) {
                                NamedNodeMap spawningItemAttributes = spawnItem.getAttributes();
                                Node spawningItemNameAttribute = spawningItemAttributes.getNamedItem("name");

                                if (spawningItemNameAttribute.getNodeValue().equals(spawningItem)) {
                                    //System.out.println("      found  " + spawningItem);

                                    returnSpawingItem.setName(spawningItemAttributes.getNamedItem("name").getNodeValue());
                                    returnSpawingItem.setModel(spawningItemAttributes.getNamedItem("model").getNodeValue());
                                    returnSpawingItem.setSound(spawningItemAttributes.getNamedItem("sound").getNodeValue());
                                    returnSpawingItem.setEmptySound(spawningItemAttributes.getNamedItem("empty-sound").getNodeValue());
                                    returnSpawingItem.setCooldown(Float.parseFloat(spawningItemAttributes.getNamedItem("cooldown").getNodeValue()));
                                }
                            }
                        }
                    }
                }
            }
        }

        return returnSpawingItem;
    }

    /* get details for a specific spawning item*/
    public InfluenceItemInventory getInfluenceItem(String influenceItem) {
        Node rootNode = itemsFile.getDocumentElement();
        InfluenceItemInventory returnInfluenceItem = new InfluenceItemInventory();

        NodeList items = rootNode.getChildNodes();

        for (int itemcount = 0; itemcount < items.getLength(); itemcount++) {
            Node anItem = items.item(itemcount);

            if (anItem.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
                //System.out.println("  anItem name " + anItem.getNodeName());

                if (anItem.getNodeName().equals("influence-items")) {

                    NodeList influenceitems = anItem.getChildNodes();

                    for (int influenceitemscount = 0; influenceitemscount < influenceitems.getLength(); influenceitemscount++) {
                        Node anInfluenceItem = influenceitems.item(influenceitemscount);

                        if (anInfluenceItem.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
                            //System.out.println("    anInfluenceItem name " + anInfluenceItem.getNodeName());

                            if (anInfluenceItem.getNodeName().equals("influence-item")) {
                                NamedNodeMap influenceItemAttributes = anInfluenceItem.getAttributes();
                                Node spawningItemNameAttribute = influenceItemAttributes.getNamedItem("name");

                                if (spawningItemNameAttribute.getNodeValue().equals(influenceItem)) {
                                    //System.out.println("      found  " + influenceItem);
                                    
                                    returnInfluenceItem.setName(influenceItemAttributes.getNamedItem("name").getNodeValue());
                                    returnInfluenceItem.setInfluence(influenceItemAttributes.getNamedItem("influence").getNodeValue());
                                    returnInfluenceItem.setMechanism(influenceItemAttributes.getNamedItem("mechanism").getNodeValue());
                                    returnInfluenceItem.setModel(influenceItemAttributes.getNamedItem("model").getNodeValue());
                                    returnInfluenceItem.setPath(influenceItemAttributes.getNamedItem("path").getNodeValue());
                                    
                                    returnInfluenceItem.setTimeActive(Float.parseFloat(influenceItemAttributes.getNamedItem("time-active").getNodeValue()));
                                    
                                }
                            }
                        }
                    }
                }
            }
        }

        return returnInfluenceItem;
    }    
    
   /* get details for a specific Influence item*/
    public InfluenceInventory getInfluence(String influence) {
        Node rootNode = itemsFile.getDocumentElement();
        InfluenceInventory returnInfluence = new InfluenceInventory();

        NodeList items = rootNode.getChildNodes();

        for (int itemcount = 0; itemcount < items.getLength(); itemcount++) {
            Node anItem = items.item(itemcount);

            if (anItem.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
                //System.out.println("  anItem name " + anItem.getNodeName());

                if (anItem.getNodeName().equals("influences")) {

                    NodeList influences = anItem.getChildNodes();

                    for (int influencescount = 0; influencescount < influences.getLength(); influencescount++) {
                        Node anInfluence = influences.item(influencescount);

                        if (anInfluence.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
                            //System.out.println("    anInfluence name " + anInfluence.getNodeName());

                            if (anInfluence.getNodeName().equals("influence")) {
                                NamedNodeMap influenceAttributes = anInfluence.getAttributes();
                                Node influenceNameAttribute = influenceAttributes.getNamedItem("name");

                                if (influenceNameAttribute.getNodeValue().equals(influence)) {
                                    //System.out.println("      found  " + influence);
                                    
                                    returnInfluence.setName(influenceAttributes.getNamedItem("name").getNodeValue());
                                    returnInfluence.setEffect(influenceAttributes.getNamedItem("effect").getNodeValue());
                                    returnInfluence.setEffectAmount(Float.parseFloat(influenceAttributes.getNamedItem("effect-amount").getNodeValue()));
                                    returnInfluence.setEffectLife(Float.parseFloat(influenceAttributes.getNamedItem("effect-life").getNodeValue()));
                                    returnInfluence.setEffectRange(Float.parseFloat(influenceAttributes.getNamedItem("effect-range").getNodeValue()));
                                    returnInfluence.setMethod(influenceAttributes.getNamedItem("method").getNodeValue());
                                }
                            }
                        }
                    }
                }
            }
        }

        return returnInfluence;
    }      
    
    /* get all influence item names for a specific spawning item*/
    public ArrayList<String> getInfluenceItems(String spawningItem) {
        Node rootNode = itemsFile.getDocumentElement();
        ArrayList<String> returnList = new <String>ArrayList();

        NodeList items = rootNode.getChildNodes();

        for (int itemcount = 0; itemcount < items.getLength(); itemcount++) {
            Node anItem = items.item(itemcount);

            if (anItem.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
                //System.out.println("  anItem name " + anItem.getNodeName());

                if (anItem.getNodeName().equals("spawning-items")) {

                    NodeList spawnitems = anItem.getChildNodes();

                    for (int spawnitemscount = 0; spawnitemscount < spawnitems.getLength(); spawnitemscount++) {
                        Node spawnItem = spawnitems.item(spawnitemscount);

                        if (spawnItem.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
                            //System.out.println("    spawnItem name " + spawnItem.getNodeName());

                            if (spawnItem.getNodeName().equals("spawning-item")) {
                                NamedNodeMap spawningItemAttributes = spawnItem.getAttributes();
                                Node spawningItemNameAttribute = spawningItemAttributes.getNamedItem("name");

                                if (spawningItemNameAttribute.getNodeValue().equals(spawningItem)) {
                                    //System.out.println("      found  " + spawningItem);

                                    NodeList spawnsNodes = spawnItem.getChildNodes();

                                    for (int spawnsnodescount = 0; spawnsnodescount < spawnsNodes.getLength(); spawnsnodescount++) {
                                        Node spawns = spawnsNodes.item(spawnsnodescount);

                                        if (spawns.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
                                            NamedNodeMap spawnsAttributes = spawns.getAttributes();
                                            Node influenceItemAttribute = spawnsAttributes.getNamedItem("influence-item");

                                            //System.out.println("        Adding " + influenceItemAttribute.getNodeValue());

                                            returnList.add(influenceItemAttribute.getNodeValue());
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return returnList;
    }

    public ArrayList<String> getAllInfluenceItems() {
        //System.out.println("getAllInfluenceItems");
        Node rootNode = itemsFile.getDocumentElement();
        ArrayList<String> returnList = new <String>ArrayList();

        NodeList items = rootNode.getChildNodes();

        for (int itemcount = 0; itemcount < items.getLength(); itemcount++) {
            Node anItem = items.item(itemcount);

            if (anItem.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
                //System.out.println("  anItem name " + anItem.getNodeName());

                if (anItem.getNodeName().equals("influence-items")) {

                    NodeList influenceitems = anItem.getChildNodes();

                    for (int influenceitemscount = 0; influenceitemscount < influenceitems.getLength(); influenceitemscount++) {
                        Node influenceItem = influenceitems.item(influenceitemscount);

                        if (influenceItem.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
                            //System.out.println("    influenceItem name " + influenceItem.getNodeName());

                            if (influenceItem.getNodeName().equals("influence-item")) {
                                NamedNodeMap influenceItemAttributes = influenceItem.getAttributes();
                                Node influenceItemNameAttribute = influenceItemAttributes.getNamedItem("name");

                                //System.out.println("        Adding " + influenceItemNameAttribute.getNodeValue());

                                returnList.add(influenceItemNameAttribute.getNodeValue());
                            }
                        }
                    }
                }
            }
        }

        return returnList;
    }

    /* recurse through the DOM */
    public void debugInGameObjectTemplates(org.w3c.dom.Node node, String indent) {
        // do something with the current node instead of System.out
        //System.out.println(indent + node.getNodeName());

        NodeList nodeList = node.getChildNodes();
        for (int i = 0; i < nodeList.getLength(); i++) {
            org.w3c.dom.Node currentNode = nodeList.item(i);

            if (currentNode.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
                //calls this method for all the children which is Element
                NamedNodeMap attributes = currentNode.getAttributes();

                for (int j = 0; j < attributes.getLength(); j++) {
                    //System.out.println(indent + "  " + attributes.item(j).getNodeName() + " = " + attributes.item(j).getNodeValue());
                }

                debugInGameObjectTemplates(currentNode, indent + "  ");
            }
        }
    }

}
