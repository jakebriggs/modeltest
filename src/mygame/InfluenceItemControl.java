package mygame;

import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.control.BetterCharacterControl;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.export.JmeExporter;
import com.jme3.export.JmeImporter;
import com.jme3.export.Savable;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;
import com.jme3.scene.control.Control;
import java.io.IOException;

public class InfluenceItemControl extends AbstractControl implements Savable, Cloneable {

    private Float updateNum = 0f;
    public Boolean activated = false;
    private BulletAppState bulletAppState;
    private InfluenceManager influenceManager;
    private float speed = 1100f;
    private InfluenceItemInventory influenceItem;
    private Vector3f direction;

    public InfluenceItemControl(BulletAppState bulletAppState, InfluenceManager influenceManager, InfluenceItemInventory influenceItemInventory, Vector3f direction) {
        this.bulletAppState = bulletAppState;
        this.influenceManager = influenceManager;
        this.influenceItem = influenceItemInventory;

        this.direction = direction;
    } // empty serialization constructor

    /**
     * This method is called when the control is added to the spatial, and when
     * the control is removed from the spatial (setting a null value). It can be
     * used for both initialization and cleanup.
     */
    @Override
    public void setSpatial(Spatial spatial) {
        super.setSpatial(spatial);
        /* Example:
         if (spatial != null){
         // initialize
         }else{
         // cleanup
         }
         */
    }

    /**
     * Implement your spatial's behaviour here. From here you can modify the
     * scene graph and the spatial (transform them, get and set userdata, etc).
     * This loop controls the spatial while the Control is enabled.
     *
     * @param tpf
     */
    @Override
    protected void controlUpdate(float tpf) {
        if (!activated) {
            if (updateNum > influenceItem.getTimeActive()) {
                //System.out.println("ACTIVATED " + updateNum);
                activated = true;
                /* Make a sound here for explosion, or bang or whatever */

                influenceManager.spawn(spatial.getWorldTranslation(), this.direction, influenceItem.getInfluence());
                spatial.getControl(RigidBodyControl.class).destroy();
                bulletAppState.getPhysicsSpace().removeAll(spatial);
                spatial.removeFromParent();
            } else {
                //System.out.println("NOT ACTIVATED " + updateNum);
            }

            updateNum += tpf;
        }
    }

    @Override
    public Control cloneForSpatial(Spatial spatial) {
        final InfluenceItemControl control = new InfluenceItemControl(bulletAppState, influenceManager, influenceItem, this.direction);
        /* Optional: use setters to copy userdata into the cloned control */
        // control.setIndex(i); // example
        //System.out.println("spatial is " + spatial);
        control.setSpatial(spatial);
        return control;
    }

    @Override
    protected void controlRender(RenderManager rm, ViewPort vp) {
        /* Optional: rendering manipulation (for advanced users) */
    }

    @Override
    public void read(JmeImporter im) throws IOException {
        super.read(im);
        // im.getCapsule(this).read(...);
    }

    @Override
    public void write(JmeExporter ex) throws IOException {
        super.write(ex);
        // ex.getCapsule(this).write(...);
    }

}
