/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.asset.AssetManager;
import com.jme3.bounding.BoundingBox;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.bullet.control.BetterCharacterControl;
import com.jme3.bullet.control.VehicleControl;
import com.jme3.bullet.util.CollisionShapeFactory;
import com.jme3.input.InputManager;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.math.FastMath;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.renderer.queue.RenderQueue;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.terrain.geomipmap.TerrainQuad;
import java.util.UUID;
import com.jme3.math.Matrix3f;

/**
 *
 * @author Jacob
 */
public class Rideable {

    Node rideableNode;

    String type;
    RideableManager rideableManager;
    AssetManager assetManager;
    Node nodeToAttachTo;
    Vector3f startlocation;
    InGameObjectTemplateManager inGameObjectTemplateManager;
    UUID id;
    BulletAppState bulletAppState;
    private float wheelRadius;
    private float steeringValue = 0;
    private float accelerationValue = 0;
    UserSettings userSettings;
    Integer joystickDevice;
    InputManager inputManager;
    VehicleControl vehicleControl;
    Boolean beingRidden = false;

    public Rideable(RideableManager rideableManager, BulletAppState bulletAppState, InputManager inputManager, AssetManager assetManager, Node nodeToAttachTo, Vector3f startlocation, UserSettings userSettings, Integer joystickDevice, InGameObjectTemplateManager inGameObjectTemplateManager, String type, UUID id) {

        this.type = type;
        this.rideableManager = rideableManager;
        this.assetManager = assetManager;
        this.nodeToAttachTo = nodeToAttachTo;
        this.startlocation = startlocation;
        this.inGameObjectTemplateManager = inGameObjectTemplateManager;
        this.bulletAppState = bulletAppState;
        this.id = id;
        this.inputManager = inputManager;
        this.userSettings = userSettings;
        this.joystickDevice = joystickDevice;

        String model;

        if (type == null) {
            type = "generic";
        }

        if (type.equals("car")) {
            model = "Models/Ferrari/Car.scene";
        } else {
            model = "Models/Ferrari/Car.scene";
        }

        /**
         * *********************************************************************
         */
        float stiffness = 120.0f;//200=f1 car
        float compValue = 0.2f; //(lower than damp!)
        float dampValue = 0.3f;
        final float mass = 800;

        // fancycar Load model and get chassis Geometry
        rideableNode = (Node) assetManager.loadModel("Models/Ferrari/Car.scene");
        //rideableNode.setName(id.toString());
        Geometry chasis = findGeom(rideableNode, "Car");
        BoundingBox box = (BoundingBox) chasis.getModelBound();

        //Create a hull collision shape for the chassis
        CollisionShape carHull = CollisionShapeFactory.createDynamicMeshShape(chasis);

        rideableNode.setShadowMode(RenderQueue.ShadowMode.Cast);

        //Create a vehicle control
        vehicleControl = new VehicleControl(carHull, mass);

        rideableNode.addControl(vehicleControl);

        //Setting default values for wheels
        vehicleControl.setSuspensionCompression(compValue * 2.0f * FastMath.sqrt(stiffness));
        vehicleControl.setSuspensionDamping(dampValue * 2.0f * FastMath.sqrt(stiffness));
        vehicleControl.setSuspensionStiffness(stiffness);
        vehicleControl.setMaxSuspensionForce(10000);

        //Create four wheels and add them at their locations
        //note that our fancy car actually goes backwards..
        Vector3f wheelDirection = new Vector3f(0, -1, 0);
        Vector3f wheelAxle = new Vector3f(-1, 0, 0);

        Geometry wheel_fr = findGeom(rideableNode, "WheelFrontRight");
        wheel_fr.center();
        box = (BoundingBox) wheel_fr.getModelBound();
        wheelRadius = box.getYExtent();
        float back_wheel_h = (wheelRadius * 1.7f) - 1f;
        float front_wheel_h = (wheelRadius * 1.9f) - 1f;
        vehicleControl.addWheel(wheel_fr.getParent(), box.getCenter().add(0, -front_wheel_h, 0),
                wheelDirection, wheelAxle, 0.2f, wheelRadius, true);

        Geometry wheel_fl = findGeom(rideableNode, "WheelFrontLeft");
        wheel_fl.center();
        box = (BoundingBox) wheel_fl.getModelBound();
        vehicleControl.addWheel(wheel_fl.getParent(), box.getCenter().add(0, -front_wheel_h, 0),
                wheelDirection, wheelAxle, 0.2f, wheelRadius, true);

        Geometry wheel_br = findGeom(rideableNode, "WheelBackRight");
        wheel_br.center();
        box = (BoundingBox) wheel_br.getModelBound();
        vehicleControl.addWheel(wheel_br.getParent(), box.getCenter().add(0, -back_wheel_h, 0),
                wheelDirection, wheelAxle, 0.2f, wheelRadius, false);

        Geometry wheel_bl = findGeom(rideableNode, "WheelBackLeft");
        wheel_bl.center();
        box = (BoundingBox) wheel_bl.getModelBound();
        vehicleControl.addWheel(wheel_bl.getParent(), box.getCenter().add(0, -back_wheel_h, 0),
                wheelDirection, wheelAxle, 0.2f, wheelRadius, false);

        vehicleControl.getWheel(0).setFrictionSlip(20);
        vehicleControl.getWheel(1).setFrictionSlip(20);
        vehicleControl.getWheel(2).setFrictionSlip(20);
        vehicleControl.getWheel(3).setFrictionSlip(20);

//        RideableControl control = new RideableControl(inputManager, userSettings, joystickDevice, inGameObjectTemplateManager, assetManager);
//        rideableNode.addControl(control);
        nodeToAttachTo.attachChild(rideableNode);
        rideableNode.setLocalTranslation(startlocation.x, -10, startlocation.z);

        bulletAppState.getPhysicsSpace().add(vehicleControl);

        initKeys();
    }

    Vector3f getLocalTranslation() {
        return rideableNode.getLocalTranslation();
    }

    private Geometry findGeom(Spatial spatial, String name) {
        if (spatial instanceof Node) {
            Node node = (Node) spatial;
            for (int i = 0; i < node.getQuantity(); i++) {
                Spatial child = node.getChild(i);
                Geometry result = findGeom(child, name);
                if (result != null) {
                    return result;
                }
            }
        } else if (spatial instanceof Geometry) {
            if (spatial.getName().startsWith(name)) {
                return (Geometry) spatial;
            }
        }
        return null;
    }

    private void initKeys() {
        inputManager.addMapping("Reset", new KeyTrigger(userSettings.getInteger("keyboardMapJump")));

        inputManager.addListener(actionListener, "Left");
        inputManager.addListener(actionListener, "Right");
        inputManager.addListener(actionListener, "Up");
        inputManager.addListener(actionListener, "Down");
        inputManager.addListener(actionListener, "Reset");
        System.out.println("RIDEABLE initkeys");
    }

    public Boolean getBeingRidden() {
        return beingRidden;
    }

    public void setBeingRidden(Boolean beingRidden) {
        this.beingRidden = beingRidden;
    }

    private ActionListener actionListener = new ActionListener() {
        public void onAction(String name, boolean keyPressed, float tpf) {
            
            if (beingRidden) {
                System.out.println("RIDEABLE Key pressed onAction name |" + name + "| keyPressed | " + keyPressed + "|");
                
                if (name.equals("Left")) {
                    if (keyPressed) {
                        steeringValue += .5f;
                    } else {
                        steeringValue += -.5f;
                    }
                    vehicleControl.steer(steeringValue);
                } else if (name.equals("Right")) {
                    if (keyPressed) {
                        steeringValue += -.5f;
                    } else {
                        steeringValue += .5f;
                    }
                    vehicleControl.steer(steeringValue);
                } //note that our fancy car actually goes backwards..
                else if (name.equals("Up")) {
                    if (keyPressed) {
                        accelerationValue -= 2400;
                    } else {
                        accelerationValue += 2400;
                    }
                    vehicleControl.accelerate(accelerationValue);
                    //spatial.getControl(VehicleControl.class).setCollisionShape(CollisionShapeFactory.createDynamicMeshShape(findGeom(carNode, "Car")));
                } else if (name.equals("Down")) {
                    if (keyPressed) {
                        vehicleControl.brake(40f);
                    } else {
                        vehicleControl.brake(0f);
                    }
                } else if (name.equals("Reset")) {
                    if (keyPressed) {
                        System.out.println("Reset");
                        vehicleControl.setPhysicsLocation(Vector3f.ZERO);
                        vehicleControl.setPhysicsRotation(new Matrix3f());
                        vehicleControl.setLinearVelocity(Vector3f.ZERO);
                        vehicleControl.setAngularVelocity(Vector3f.ZERO);
                        vehicleControl.resetSuspension();
                    } else {
                    }
                }

            }
        }
    };
}
