/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.asset.AssetManager;
import com.jme3.bullet.BulletAppState;
import com.jme3.input.InputManager;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.system.AppSettings;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;
import java.util.UUID;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;

/**
 *
 * @author Jacob
 */
public class RideableManager {

    private Node rideableNode;
    private BulletAppState bulletAppState;
    private AssetManager assetManager;
    // private InfluenceItemManager influenceItemManager;
    private InGameObjectTemplateManager inGameObjectTemplateManager;
    private String settingsfile = "InitialGameState/Rideable.xml";
    InputManager inputManager;
    UserSettings userSettings;
    Integer joystickDevice;
    private ArrayList<Rideable> rideables = new ArrayList<Rideable>();

    public RideableManager(Node rideableNode, BulletAppState bulletAppState, AssetManager assetManager, InputManager inputManager, UserSettings userSettings, Integer joystickDevice, InGameObjectTemplateManager inGameObjectTemplateManager) {
        this.rideableNode = rideableNode;
        this.assetManager = assetManager;
        this.inGameObjectTemplateManager = inGameObjectTemplateManager;
        this.bulletAppState = bulletAppState;
        this.inputManager = inputManager;
        this.userSettings = userSettings;
        this.joystickDevice = joystickDevice;
    }

    public void load() {
        Document rideableFile = (Document) assetManager.loadAsset(settingsfile);
        processRideable(rideableFile.getDocumentElement());
        //randomStaticModels(100);

        System.out.println("rideableManager LOADED");
    }

    public Rideable getClosestRideable(){
        return rideables.get(0);
    }
    
    
    public void spawn(Vector3f location, String faction, String type) {
                                                                                                                                                             
        rideables.add(new Rideable(this, bulletAppState, inputManager, assetManager, rideableNode, location, userSettings, joystickDevice, inGameObjectTemplateManager,type,UUID.randomUUID()));
    }

    /* recurse throught he DOM */
    public void processRideable(org.w3c.dom.Node node) {
        // do something with the current node instead of System.out
        System.out.println(node.getNodeName());

        if (node.getNodeName() == "rideable") {
            NamedNodeMap attributes = node.getAttributes();

            Float x = Float.parseFloat(attributes.getNamedItem("x").getNodeValue());
            Float y = Float.parseFloat(attributes.getNamedItem("y").getNodeValue());
            Float z = Float.parseFloat(attributes.getNamedItem("z").getNodeValue());
            String type = attributes.getNamedItem("type").getNodeValue();

            rideables.add(new Rideable(this, bulletAppState, inputManager, assetManager, rideableNode, new Vector3f(x, y, z), userSettings, joystickDevice, inGameObjectTemplateManager, type, UUID.randomUUID()));
        }

        NodeList nodeList = node.getChildNodes();
        for (int i = 0; i < nodeList.getLength(); i++) {
            org.w3c.dom.Node currentNode = nodeList.item(i);
            if (currentNode.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
                //calls this method for all the children which is Element
                processRideable(currentNode);
            }
        }
    }

    public Character findStaticModel(UUID id) {
        Iterator it = rideables.iterator();

        while (it.hasNext()) {
            NPCCharacter next = (NPCCharacter) it.next();

            if (next.getId().equals(id)) {
                return next;
            }
        }

        return null;
    }

    public void manyStaticObjectsInfluenced(HashSet<UUID> ids, InfluenceInventory influence) {
        Iterator it = ids.iterator();

        while (it.hasNext()) {
            staticObjectInfluenced((UUID) it.next(), influence);
        }
    }

    public void staticObjectInfluenced(UUID id, InfluenceInventory influence) {
        /* very lazy way to find influenced character */
        System.out.println("staticObjectInfluenced " + id);

        findStaticModel(id).causeInfluence(influence);
//        Iterator it = staticModels.iterator();
//
//        while (it.hasNext()) {
//            NPCCharacter next = (NPCCharacter) it.next();
//
//            if (next.getId().equals(id)) {
//                next.causeInfluence(influence);
//            }
//        }
    }


}
