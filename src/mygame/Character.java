/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.asset.AssetManager;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.control.BetterCharacterControl;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Box;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.UUID;

/**
 *
 * @author Jacob
 */
public class Character {

    Node characterNode;

    InfluenceItemManager influenceItemManager;
    CharacterManager characterManager;
    String type;
    UUID id;
    ArrayList<InfluenceItemInventory> influenceItems;
    ArrayList<SpawningItemInventory> spawningItems;
    Float damage = 100f;
    Float stamina = 100f;
    Float affinity = 0f;
            
    public Character(CharacterManager characterManager, BulletAppState bulletAppState, AssetManager assetManager, InfluenceItemManager influenceItemManager, Node nodeToAttachTo, Vector3f location, String type, InGameObjectTemplateManager inGameObjectTemplateManager, UUID id) {
        ArrayList<String> allInfluenceItems = inGameObjectTemplateManager.getAllInfluenceItems();
        this.id = id;
        this.influenceItems = new <InfluenceItemInventory>ArrayList();

        Iterator it = allInfluenceItems.iterator();

        while (it.hasNext()) {

            InfluenceItemInventory influenceItem = inGameObjectTemplateManager.getInfluenceItem((String) it.next());
            influenceItem.setAmount(10);
            influenceItems.add(influenceItem);

        }

        this.spawningItems = new <SpawningItemInventory>ArrayList();

        this.type = type;
        String model;

        if (type == null) {
            type = "generic";
        }

        if (type.equals("generic")) {
            model = "Models/Oto/Oto.mesh.xml";
        } else if (type.equals("player")) {
            model = "Models/Oto/Oto.mesh.xml";
        } else if (type.equals("commander")) {
            model = "Models/Oto/Oto.mesh.xml";
        } else if (type.equals("kangaroo")) {
            model = "Models/Oto/Oto.mesh.xml";
        } else {
            model = "Models/" + type + "/" + type + ".mesh.xml";
        }

        Spatial spatial = assetManager.loadModel(model);
        spatial.setName(type + " spatial ");
        Node spatialNode = (Node) spatial;
        spatialNode.setName(type + "spatialNode " + UUID.randomUUID().toString());

        characterNode = new Node(id.toString());
        characterNode.attachChild(spatialNode);

        characterNode.move(5, 3.5f, 5);

//        AnimControl animControl = spatialNode.getControl(AnimControl.class);
//        animControl.addListener(animEventListener);
//
//        AnimChannel animControlChannel = animControl.createChannel();
//        animControlChannel.setAnim("stand");
        BetterCharacterControl betterCharacterControl = new BetterCharacterControl(5f, 5f, 80);

        characterNode.addControl(betterCharacterControl);

        betterCharacterControl.setJumpForce(new Vector3f(0, 5f, 0));
        betterCharacterControl.setGravity(new Vector3f(0, 1f, 0));
        betterCharacterControl.setApplyPhysicsLocal(true);
        betterCharacterControl.warp(location); // warp character into landscape at particular location

        bulletAppState.getPhysicsSpace().add(betterCharacterControl);
        bulletAppState.getPhysicsSpace().addAll(characterNode);

        nodeToAttachTo.attachChild(characterNode);

        this.influenceItemManager = influenceItemManager;

        /* every character can spawn stuff with their hands */
        spawningItems.add(inGameObjectTemplateManager.getSpawingItem("hand"));
    }

    public Vector3f getLocalTranslation() {
        return characterNode.getLocalTranslation();
    }

    public void setWalkDirection(Vector3f direction) {
        characterNode.getControl(BetterCharacterControl.class).setWalkDirection(direction);
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public void causeInfluence(InfluenceInventory influence) {
        System.out.println(System.currentTimeMillis() + " Causing " + influence.getEffectAmount() + " for tpf " + influence.getTpf() + " (" + influence.getEffectAmount() * influence.getTpf() + ") " + influence.getEffect() + " to " + id);
        Boolean increase = false;
        
        Float effectPerSecond = influence.getEffectAmount() * influence.getTpf();

        if (effectPerSecond > 0) {
            increase = true;
        }

        
        
        if (influence.getEffect().equals("damage")) {
            if ((increase && damage < 100) || (!increase && damage > 0)) {
                damage = damage + effectPerSecond;
            }

            if (damage < 0) {
                damage = 0f;
            } else if (damage > 100) {
                damage = 100f;
            }
        } else if (influence.getEffect().equals("stamina")) {
            if ((increase && stamina < 100) || (!increase && stamina > 0)) {
                stamina = stamina + effectPerSecond;
            }

            if (stamina < 0) {
                stamina = 0f;
            } else if (stamina > 100) {
                stamina = 100f;
            }
        } else if (influence.getEffect().equals("affinity")) {
            if ((increase && affinity < 100) || (!increase && affinity > 0)) {
                affinity = affinity + effectPerSecond;
            }

            if (affinity < 0) {
                affinity = 0f;
            } else if (affinity > 100) {
                affinity = 100f;
            }
        }
//
//        if (damage <= 0) {
//            System.out.println("DEAD " + id);
//        }
//        if (stamina <= 0) {
//            System.out.println("KNOCKEDOUT " + id);
//        }

    }

    public Float getDamage() {
        return damage;
    }

    public void setDamage(Float damage) {
        this.damage = damage;
    }

    public Float getStamina() {
        return stamina;
    }

    public void setStamina(Float stamina) {
        this.stamina = stamina;
    }

    public Float getAffinity() {
        return affinity;
    }

    public void setAffinity(Float affinity) {
        this.affinity = affinity;
    }

//    private AnimEventListener animEventListener = new AnimEventListener() {
//
//        public void onAnimCycleDone(AnimControl control, AnimChannel channel, String animName) {
//            System.out.println("onAnimCycleDone Not supported yet");
//            //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//        }
//
//        public void onAnimChange(AnimControl control, AnimChannel channel, String animName) {
//            System.out.println("onAnimChange Not supported yet");
//        }
//    };


}
